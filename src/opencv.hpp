#ifndef __aidecv__
#define __aidecv__

#include "std.hpp"
#include "Value.hpp"

#include <opencv2/core/core.hpp>

namespace aidecv {
  /**
   * @class aidecv
   * @description  Implements a few opencv utilities or specific algorithms used in this context.
   * - These functions are accessible via the `aidecv::` prefix.
   */

  /**
   * @function mat2mat
   * @memberof aidecv
   * @static
   * @description Returns a cv::Mat temporary matrix that can be saved or shown.
   * - `CV_32FC2` is converted to `CV_32FC3` since only 1, 3 or 4 channels image can be saved.
   * - `CV_32SC1` is converted to `CV_32FC1` for proper management of the values.
   * - `CV_8UC1`, `CV_8UC3`, and `CV_32FC3` are left unchanged.
   * - other `cv::Mat` formats are not used in this context.
   * @param {Mat} img The input image.
   * @return {Mat} A temporary reference to the same image contents, but in a savable or showable form.
   */
  const cv::Mat& mat2mat(const cv::Mat& img);

  /**
   * @function mat2json
   * @memberof aidecv
   * @static
   * @description Returns the cv::Mat characteristics as a string, with optionally pixels mean and standard-deviation.
   * @param {Mat} img The input image.
   * @param {bool} [meanstdev=true] If true computes pixels intensity mean and standard-deviation.
   * @return {JSON} A temporary JSON data structure with the different parameters.
   */
  JSON mat2json(const cv::Mat& img, bool meanstdev = true);

  /**
   * @function mat2txt
   * @memberof aidecv
   * @static
   * @description Returns the image data in textual form.
   * @param {Mat} img The input image.
   * @return {String} A string with a 2D print of the image pixel values.
   */
  std::string mat2txt(const cv::Mat& img);

  /**
   * @function bgr2bin
   * @memberof aidecv
   * @static
   * @description Converts a BGR image to a binary one with automatic threshold calculation.
   * @param {Mat} img The input image.
   * @return {Mat} A temporary reference to the computed image.
   */
  const cv::Mat& bgr2bin(const cv::Mat& img);

  /**
   * @class aidecv~opencvwindow
   * @memberof aidecv
   * @description Implements a registration mechanism to correctly manage `cv::imshow` display.
   */
  class opencvwindow {
    // Gets the window name buffer
    static std::vector < std::string >& getWindowNames();
public:

    /**
     * @function show
     * @memberof aidecv~opencvwindow
     * @static
     * @description Shows the window and registers its name.
     * @param {string} name The window name.
     * @param {Mat} img The image to show.
     */
    static void show(String name, const cv::Mat& img);

    /**
     * @function waitUntilWindowsClosed
     * @memberof aidecv~opencvwindow
     * @static
     * @description Waits untill window are closed by the user.
     * - Usually used in a contsruct of the form:
     * ```
     * include "opencv.hpp"
     *
     * int main(int argc, char *argv[]) {
     *  // ../..
     *  aidecv::opencvwindow::waitUntilWindowsClosed();
     * }
     * ```
     * thus called at the end of the program, allowing to maintain the program until all windows are closed.
     */
    static void waitUntilWindowsClosed();
  };
}
#endif
