#include "Image.hpp"
#include "Setter.hpp"

namespace aidecv {
  std::map < std::string, Setter * > &Setter::getSetters() {
    static std::map < std::string, Setter * > setters;
    return setters;
  }
  Setter::Setter(String what)
  {
    getSetters()[what] = this;
  }
  void Setter::set(Image& target, JSON command, const Image& source)
  {
    aidesys::alert("illegal-state", "aidecv::Image::Setter::set, not implemented");
  }
}
