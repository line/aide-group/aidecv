@aideAPI

This package provides an abstract interface with [opencv](https://docs.opencv.org/4.0.0) image processing functions and additional algorithms at a symbolic level to be used, e.g., via a web interface.
