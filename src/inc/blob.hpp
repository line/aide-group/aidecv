#include "Setter.hpp"

namespace aidecv {
  class BlobSetter: public Setter {
public:
    BlobSetter() : Setter("blob") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      target.set("hsv");
      target.parameters["blob"] = blob(target.channels["hsv"],
                                       target.channels["blob"],
                                       command.get("blue", 128),
                                       command.get("green", 128),
                                       command.get("red", 128),
                                       command.get("hue_threshold", 10),
                                       command.get("saturation_threshold", 0.0f),
                                       command.get("value_threshold", 0.0f));
    }
    /**
     * @function BlobSetter::blob
     * @memberof aidecv
     * @static
     * @description Blob algorithm implementation for a CV_32FC3 contiguous HSV buffer image
     * @param {Mat} source The input HSV `CV_32FC3` image.
     * @param {Mat} result The output blob mask `CV_32SC1` image, with:
     * - `1`: for blob pixel value,
     * - `0`: for non-blob pixel value.
     * @param {uint} blue The blob median blue color, between 0 and 255, default is 128.
     * @param {uint} green The blob median green color, between 0 and 255, default is 128.
     * @param {uint} red The blob median red color, between 0 and 255, default is 128.
     * @param {uint} hue_threshold The color hue threshold, in degree, in [0deg, 180deg], values in [hue_blob - hue_threshold, hue_blob + hue_threshold] are considered, default is 10deg.
     * @param {uint} saturation_threshold The color saturation minimal value, in percentage, values between [saturation_threshold, 100%] are considered, default is 0%.
     * @param {uint} value_threshold The intensity minimal value, values between [intensity_threshold, 255] are considered, default is 0.
     * @return {Value} A `wjson::Value` with the blob `size` and center `(x, y)` coordinates, if defined.
     */
    static wjson::Value blob(const cv::Mat& source, cv::Mat& result, unsigned int blue = 128, unsigned int green = 128, unsigned int red = 128, unsigned int hue_threshold = 10, unsigned int saturation_threshold_ = 0, unsigned int value_threshold_ = 0)
    {
      // Calculates the blob hue
      float hue_blob;
      {
        cv::Mat3f brg_blob(cv::Vec3f (blue, green, red)), hsv_blob;
        cv::cvtColor(brg_blob, hsv_blob, cv::COLOR_BGR2HSV);
        hue_blob = hsv_blob(0, 0)[0];
      }
      // Sets the parameters
      bool hue_sigma[2 * 360 + 1];
      {
        for(int h = -360; h <= 360; h++) {
          hue_sigma[360 + h] = (abs(h) < 180.0 ? abs(h) : 360.0 - abs(h)) < hue_threshold;
        }
      }
      float saturation_threshold = saturation_threshold_ / 100.0;
      float value_threshold = value_threshold_ / 256.0;
      // Threshold algorithm implementation for a CV_32FC3 contiguous buffer HSV image
      {
        unsigned int width = source.cols, height = source.rows;
        const float *source_ptr = source.ptr < float > ();
        result = cv::Mat(cv::Size(width, height), CV_32SC1, cv::Scalar(0));
        int *ptr_result = result.ptr < int > ();
        // Loops on the image
        double m0 = 0, m1_i = 0, m1_j = 0;
        for(unsigned int j = 0, ij = 0, ijc = 0; j < height; j++) {
          for(unsigned int i = 0; i < width; i++, ij++, ijc += 3) {
            if(hue_sigma[360 + (int) (source_ptr[ijc] - hue_blob)] &&
               saturation_threshold <= source_ptr[ijc + 1] &&
               value_threshold <= source_ptr[ijc + 2])
            {
              ptr_result[ij] = 1;
              m0++, m1_i += i, m1_j += j;
            }
          }
        }
        // Reports the results
        {
          wjson::Value results;
          results["size"] = m0;
          if(m0 > 0) {
            results["x"] = m1_i / m0, results["y"] = m1_j / m0;
          }
          return results;
        }
      }
    }
  }
  blobSetter;
}
