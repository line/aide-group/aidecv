#include "Setter.hpp"

#include <opencv2/imgproc.hpp>

namespace aidecv {
  class SubtractSetter: public Setter {
public:
    SubtractSetter() : Setter("subtract") {}
    void set(Image& target, JSON command, const Image& source)
    {
      subtract(target.channels["subtract"] = target.channels["input"], source.channels.at("input"), target.channels["input"], command.get("threshold", 50), command.get("mode", "step"));
    }
    /**
     * @function SubtractSetter::subtract
     * @memberof aidecv
     * @static
     * @description Implements the multiplicative subtraction of a background.
     * - The computation writes: dst = img * gain(|img - src|)
     * @param {Mat} img The computation input image.
     * @param {Mat} src The computation reference, i.e., the ``background´´ image.
     * @param {Mat} dst The computation output image.
     * @param {uint} threshold The intensity threshold above which it is not in the background, default is `50`.
     * @param {string} mode The threshold mechanism:
     * - `step`: To set to zero values below a given BGR threshold (default).
     * - `weak`: To progressively reduce the intensity below a given BGR threshold.
     */
    static void subtract(const cv::Mat& img, const cv::Mat& src, cv::Mat& dst, unsigned int threshold = 50, String mode = "step")
    {
      unsigned int width = img.cols, height = img.rows;
      aidesys::alert(img.cols != src.cols || img.rows != src.rows, "illegal-argument", "in aidecv::Image::set/subtract, incompatible image sizes between source (%dx%d) and target (%dx%d)", src.cols, src.rows, width, height);
      // Uses look-up table to speed calculation
      static unsigned int abs_[511], *abs = abs_ + 255;
      static double gain[3 * 255 + 1];
      {
        int done_with_threshold = -1;
        if(done_with_threshold < 0) {
          for(int i = -255; i <= 255; i++) {
            abs[i] = i < 0 ? -i : i;
          }
        }
        if(done_with_threshold != (int) threshold) {
          if(mode == "weak") {
            double g = 1.0 / fmax(1, 6 * threshold);
            for(unsigned int i = 0; i <= 3 * 255; i++) {
              double x = g * fabs(i);
              gain[i] = x < 1 ? x * x * (3 - 2 * x) : 1;
            }
          } else {
            for(unsigned int i = 0; i <= 3 * 255; i++) {
              gain[i] = i < 3 * threshold ? 0 : 1;
            }
          }
        }
        done_with_threshold = threshold;
      }
      // Computes the thresholding
      {
        dst = cv::Mat(cv::Size(width, height), CV_8UC3);
        unsigned char *dst_ptr = dst.ptr < unsigned char > ();
        const unsigned char *img_ptr = img.ptr < unsigned char > (), *src_ptr = src.ptr < unsigned char > ();
        for(unsigned int ijc = 0, ij = 0; ij < width * height; ij++) {
          unsigned int d =
            abs[img_ptr[ijc] - src_ptr[ijc]] +
            abs[img_ptr[ijc + 1] - src_ptr[ijc + 1]] +
            abs[img_ptr[ijc + 2] - src_ptr[ijc + 2]];
          double g = gain[d];
          for(unsigned int c = 0; c < 3; c++, ijc++) {
            dst_ptr[ijc] = (int) (g * img_ptr[ijc]);
          }
        }
      }
    }
  }
  subtractSetter;
}
