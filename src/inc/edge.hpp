#include "Setter.hpp"

namespace aidecv {
  class EdgeSetter: public Setter {
public:
    EdgeSetter() : Setter("edge") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      target.set(aidesys::echo("{do: smooth window: %d}", command.get("window", 1)));
      const cv::Mat& input = target.channels["smooth"];
      cv::Mat& result = target.channels["edge"];
      edge(input, result);
    }
    /**
     * @function EdgeSetter::edge
     * @memberof aidecv
     * @static
     * @description Edge algorithm implementation for a CV_32FC3 contiguous smoothed image, output an CV_32FC2 result.
     * - It is based on the local intensity derivative [SVD](https://en.wikipedia.org/wiki/Singular-value_decomposition) we obtain the [color edge eigen elements](./Image.pdf#page=3), using a smoothed image.
     * @param {Mat} img The computation input image.
     * @param {Mat} dst The computation output image.
     */
    static void edge(const cv::Mat& img, cv::Mat& dst)
    {
      unsigned int width = img.cols, height = img.rows;
      const float *img_ptr = img.ptr < float > ();
      dst = cv::Mat(cv::Size(width, height), CV_32FC2, cv::Scalar(0, 0));
      float *dst_ptr = dst.ptr < float > ();
      // Iterates on the the image, but the border
      {
        const unsigned int dijc0 = 3 * width;
        for(unsigned int j = 1, ijc0 = 3 + dijc0, ijc1 = 2 + 2 * width; j < height - 1; j++, ijc0 += 6, ijc1 += 4) {
          for(unsigned int i = 1; i < width - 1; i++, ijc1 += 2) {
            // Calculates the local derivative
            float J[3][2];
            for(unsigned int c = 0; c < 3; c++, ijc0++) {
              // -aidesys::alert(ijc0 != c + 3 * (i + width * j), "illegal-state", "loop edge : (i = %d < %d, j = %d < %d, c = %d < 3) ijc0 = %d != %d", i, width, j, height, c, ijc0, c + 3 * (i + width * j));
              J[c][0] = img_ptr[ijc0 + 3] - img_ptr[ijc0 - 3];
              J[c][1] = img_ptr[ijc0 + dijc0] - img_ptr[ijc0 - dijc0];
            }
            // -aidesys::alert(ijc1 != 2 * (i + width * j), "illegal-state", "loop edge : (i = %d < %d, j = %d < %d) ijc1 = %d != %d", i, width, j, height, ijc1, 2 * (i + width * j));
            // Calculates the 2D momenta and eigen-elements
            {
              float Jxx = J[0][0] * J[0][0] + J[1][0] * J[1][0] + J[2][0] * J[2][0];
              float Jyy = J[0][1] * J[0][1] + J[1][1] * J[1][1] + J[2][1] * J[2][1];
              dst_ptr[ijc1] = 256.0 * sqrt(Jxx + Jyy);
              if(dst_ptr[ijc1] > 1) {
                float Jxy = J[0][0] * J[0][1] + J[1][0] * J[1][1] + J[2][0] * J[2][1];
                const static float k = 180.0 / M_PI * 0.5;
                float theta = k * atan2(Jxy + Jxy, Jxx - Jyy);
                theta = theta < 0 ? 180.0 + theta : theta;
                float Jx = J[0][0] + J[1][0] + J[2][0], Jy = J[0][1] + J[1][1] + J[2][1];
                theta = Jy > 0 || (Jx > 0 && Jy == 0) ? 180.0 + theta : theta;
                dst_ptr[ijc1 + 1] = theta;
              }
            }
          }
        }
      }
    }
  }
  edgeSetter;
}
