#include "Setter.hpp"
#include "file.hpp"
#include <cctype>

#include <opencv2/imgcodecs/imgcodecs.hpp>

namespace aidecv {
  class CacheSetter: public Setter {
public:
    CacheSetter() : Setter("cache") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      aidesys::alert(!command.isMember("name"), "illegal-argument", "in aidecv::Image::set/cache, the name is undefined");
      std::string name = command.get("name", "");
      auto it = images.find(name);
      std::string what = command.get("what", it == images.end() ? "put" : "get").substr(0, 3);
      aidesys::alert(what != "put" && what != "get" && what != "del", "illegal-argument", "in aidecv::Image::set/cache, undefined '" + what + "' operation should be (put|get|del).");
      aidesys::alert(what == "get" && it == images.end(), "illegal-argument", "in aidecv::Image::set/cache::" + what + ", the '" + name + "' image is undefined");
      if(what == "put") {
        images[name] = target;
      } else if(what == "del") {
        images.erase(name);
      } else {
        if(command.get("clear", true)) {
          target.clear();
        }
        CopySetter::copy(images.at(name), target);
      }
    }
  }
  cacheSetter;
}
