#include "Setter.hpp"

namespace aidecv {
  class CopySetter: public Setter {
public:
    CopySetter() : Setter("copy") {}
    void set(Image& target, JSON command, const Image& source)
    {
      if(command.get("clear", true)) {
        target.clear();
      }
      copy(source, target);
    }
    static void copy(const Image& source, Image& target)
    {
      for(auto channel : source.channels) {
        target.channels[channel.first] = channel.second.clone();
      }
      target.parameters["width"] = source.parameters.at("width");
      target.parameters["height"] = source.parameters.at("height");
    }
  }
  copySetter;
}
