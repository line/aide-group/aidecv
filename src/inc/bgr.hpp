#ifndef __aidecv_balance__
#define __aidecv_balance__

#include "Setter.hpp"

namespace aidecv {
  class BgrSetter: public Setter {
public:
    BgrSetter() : Setter("bgr") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      target.channels["input"].convertTo(target.channels["bgr"], CV_32FC3, 1.0 / 255.0);
    }
  }
  bgrSetter;
}
#endif
