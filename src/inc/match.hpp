#include "Setter.hpp"
#include "Optimizer.hpp"

namespace aidecv {
  class MatchSetter: public Setter {
    static const std::string names[];
public:
    MatchSetter() : Setter("match") {}
    void set(Image& target, JSON command, const Image& source)
    {
      wjson::Value& params = target.parameters["match"];
      wjson::Value parameters = command.at("parameters");
      const cv::Mat& img = target.channels["input"], src = source.channels.at("input"), alpha = const_cast < Image & > (source).getAlpha();
      for(unsigned k = 0, K = parameters.length(); k < K; k++) {
        params["similarity"] = doMatch(img, src, alpha, parameters[k], parameters[k + 1]);
      }
      params["results"] = parameters;
      for(unsigned int i = 0; i < 6; i++) {
        params["transform"][names[i]] = parameters[parameters.length() - 1][names[i]]["zero"];
      }
    }
    class MappingCriterion: public stepsolver::Criterion {
      const cv::Mat& img, src, alpha;
      mutable cv::Mat A, wimg;
public:
      MappingCriterion(const cv::Mat& img, const cv::Mat& src, const cv::Mat& alpha) : img(img), src(src), alpha(alpha), A(cv::Mat(2, 3, CV_64F, 0.0)), wimg(img.clone()) {}
      double cost(const double values[]) const
      {
        // - printf("cost v = (%.2f %.2f %.2f %.2f %.2f %.2f) =>", values[0], values[1], values[2], values[3], values[4], values[5]);
        double *A_ptr = A.ptr < double > ();
        A_ptr[0] = values[3] + values[4], A_ptr[1] = values[5] - values[2], A_ptr[2] = values[0];
        A_ptr[3] = values[2] + values[5], A_ptr[4] = values[3] - values[4], A_ptr[5] = values[1];
        cv::warpAffine(img, wimg, A, img.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(255, 255, 255));
        double c = CorrelationSimilarity::getCorrelation(wimg, src, alpha);
        // - printf(" c = %f\n", c);
        return c;
      }
    };
    double doMatch(const cv::Mat& img, const cv::Mat& src, const cv::Mat& alpha, JSON input, wjson::Value& next)
    {
      static const wjson::Value empty("{ zero: 0}", true), zoom_empty("{ zero: 1}", true);
      wjson::Value mappingParameters;
      for(unsigned int i = 0; i < 6; i++) {
        mappingParameters[i] = input.isMember(names[i]) ? input.at(names[i]) : i == 3 ? zoom_empty : empty;
      }
      MappingCriterion mappingCriterion(img, src, alpha);
      stepsolver::Optimizer mappingSolver(mappingParameters, mappingCriterion);
      double values[6], c = mappingSolver.minimize(values);
      // Reports results for the next iteration if any
      for(unsigned int i = 0; i < 6; i++) {
        next[names[i]]["zero"] = values[i];
      }
      return c;
    }
  }
  matchSetter;
  const std::string MatchSetter::names[] = { "translation_i", "translation_j", "rotation", "zoom", "warp", "twist" };
}
