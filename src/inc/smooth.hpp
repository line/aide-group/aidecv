#include "Setter.hpp"

namespace aidecv {
  class SmoothSetter: public Setter {
public:
    SmoothSetter() : Setter("smooth") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      target.set("bgr");
      cv::Mat& result = target.channels["smooth"] = target.channels["bgr"].clone();
      unsigned int window = command.get("window", 1);
      smooth(result, window);
    }
    /**
     * @function SmoothSetter::smooth
     * @memberof aidecv
     * @static
     * @description Smooth algorithm implementation for a CV_32FC3 contiguous buffer image
     *    - Computes a smoothed version of this image using a [1st order exponential recursive filter](./Image.pdf#page=2), as a `CV_32FC3` 3 BGR channels float.
     * @param {Mat} img The computation input/output image.
     * @param {uint} window The smoothing window in pixel, with 90% of the signal to be in a the pixel window.
     */
    static void smooth(cv::Mat& img, unsigned int window)
    {
      unsigned int width = img.cols, height = img.rows;
      float *img_ptr = img.ptr < float > ();
      double alpha = pow(0.1, 1.0 / (double) (window < 1 ? 1 : window));
      //
      const unsigned int dijc = 3 * width;
      for(unsigned int j = 0, ijc = 3; j < height; j++, ijc += dijc + 4) {
        for(unsigned int i = 1; i < width; i++) {
          for(unsigned int c = 0; c < 3; c++, ijc++) {
            // -aidesys::alert(ijc != c + 3 * (i + width * j), "illegal-state", "loop h> : (i = %d < %d, j = %d < %d, c = %d < 3) ijc = %d != %d", i, width, j, height, c, ijc, c + 3 * (i + width * j));
            img_ptr[ijc] += alpha * (img_ptr[ijc - 3] - img_ptr[ijc]);
          }
        }
        ijc -= 4;
        for(int i = width - 2; i >= 0; i--) {
          for(int c = 2; c >= 0; c--, ijc--) {
            // -aidesys::alert(ijc != c + 3 * (i + width * j), "illegal-state", "loop h< : (i = %d < %d, j = %d < %d, c = %d < 3) ijc = %d != %d", i, width, j, height, c, ijc, c + 3 * (i + width * j));
            img_ptr[ijc] += alpha * (img_ptr[ijc + 3] - img_ptr[ijc]);
          }
        }
      }
      for(unsigned int i = 0, ijc = dijc; i < width; i++, ijc += 1 + 2 * dijc) {
        for(unsigned int j = 1; j < height; j++, ijc += dijc - 3) {
          for(unsigned int c = 0; c < 3; c++, ijc++) {
            // -aidesys::alert(ijc != c + 3 * (i + width * j), "illegal-state", "loop v> : (i = %d < %d, j = %d < %d, c = %d < 3) ijc = %d != %d", i, width, j, height, c, ijc, c + 3 * (i + width * j));
            img_ptr[ijc] += alpha * (img_ptr[ijc - dijc] - img_ptr[ijc]);
          }
        }
        ijc -= 2 * dijc - 2;
        for(int j = height - 2; j >= 0; j--, ijc -= dijc - 3) {
          for(int c = 2; c >= 0; c--, ijc--) {
            // -aidesys::alert(ijc != c + 3 * (i + width * j), "illegal-state", "loop v< : (i = %d < %d, j = %d < %d, c = %d < 3) ijc = %d != %d", i, width, j, height, c, ijc, c + 3 * (i + width * j));
            img_ptr[ijc] += alpha * (img_ptr[ijc + dijc] - img_ptr[ijc]);
          }
        }
      }
    }
  }
  smoothSetter;
}
