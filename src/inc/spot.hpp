#include "Setter.hpp"

#include "time.hpp"
#include <opencv2/video/background_segm.hpp>

namespace aidecv {
  /**
   * @class aidecv~SpotEventDetector
   * @memberof aidecv
   * @description Implements a spotter, i.e., an event detector that detects a motion over a background.
   * @param {uint} [device=0] The input device.
   * @param {uint} [foreground_threshold=500] The foreground threshold detection level.
   * @param {unit} [background_count=20] The number of frame to average for the background.
   */
  class SpotEventDetector {
    unsigned int device, background_count;
    cv::Ptr < cv::BackgroundSubtractorMOG2 > bgsubtractor;
public:
    /**
     * @member {Map} input
     * @memberof aidecv~SpotEventDetector
     * @inner
     * @description The last grabbed image, in `CV_8UC3` format..
     */
    cv::Mat input;

    /**
     * @member {Map} bgmask
     * @memberof aidecv~SpotEventDetector
     * @inner
     * @description The last background mask, in `CV_8UC1` format..
     * - Background pixels values are set to `0`, foreground values are set to `128` or `255`.
     */
    cv::Mat bgmask;

    /**
     * @member {uint} motionPercentage
     * @memberof aidecv~SpotEventDetector
     * @inner
     * @description The last proportion of moving pixels in percentage, between 0 and 100%.
     */
    unsigned int motionPercentage = 0;

    // Constructs the spotter.
    SpotEventDetector(unsigned int device = 0, unsigned int foreground_threshold = 500, unsigned int background_count = 20) : device(device), background_count(background_count), bgsubtractor(cv::createBackgroundSubtractorMOG2(background_count, foreground_threshold)) {}

    /**
     * @function backgroundRegister
     * @memberof aidecv~SpotEventDetector
     * @instance
     * @description Registers the background.
     */
    void backgroundRegister()
    {
      for(unsigned int t = 0; t < background_count; t++) {
        grabber.grab(device, input);
        bgsubtractor->apply(input, bgmask, 1);
      }
      bgsubtractor->getBackgroundImage(input);
      motionPercentage = 0;
    }
    /**
     * @function grab
     * @memberof aidecv~SpotEventDetector
     * @instance
     * @description Grabs and detect the background.
     * @return The last proportion of moving pixels in percentage, between 0 and 100%.
     */
    unsigned int grab()
    {
      grabber.grab(device, input);
      bgsubtractor->apply(input, bgmask, 0);
      // Counts the moving pixels
      {
        const unsigned char *bgmask_ptr = bgmask.ptr < unsigned char > ();
        unsigned int c = 0, size = bgmask.cols * bgmask.rows;
        for(unsigned int ij = 0; ij < size; ij++) {
          if(bgmask_ptr[ij] == 255) {
            c++;
          }
        }
        return motionPercentage = 100.0 * c / size;
      }
    }
    /**
     * @function next
     * @memberof aidecv~SpotEventDetector
     * @instance
     * @description Waits until the next stable image and returns it, thus block the program until detection or timeout.
     * @param {bool} [move_else_still=true] If true detects when above the threshold, if false detects when under the threshold.
     * @param {uint} [motion_threshold=10] The motion threshold in percentage, as provided by grab(), to detect the next event.
     * @param {uint} [watchdog_delay=0] The maximal delay to wait for an event, in milli-seconds, default is 0, i.e., waiting forever.
     * @param {uint} [stabilization_count=2] Number of frames before the event is considered as stable and not spurious.
     * @param {bool} [verbose=false] If true, prints a trace during the detection delay.
     */
    bool next(bool move_else_still, unsigned int motion_threshold, unsigned int watchdog_delay, unsigned int stabilization_count, bool verbose)
    {
      unsigned int threshold = 0, delay = 0, count = stabilization_count;
      aidesys::now(false, false);
      do {
        threshold = grab();
        delay += (unsigned int) aidesys::now(false, true);
        if(verbose) {
          aidesys::alert(true, "", "{aidecv::SpotEventDetector::next-%s motion_threshold: %d > %d stabilization_count %d > %d watchdog_delay: %d > %d}", move_else_still ? "move" : "still", motion_threshold, threshold, stabilization_count, count, watchdog_delay, delay);
        }
        count = (move_else_still ? threshold > motion_threshold : threshold < motion_threshold) ? count - 1 : stabilization_count;
      } while(count > 0 && (watchdog_delay == 0 || delay < watchdog_delay));
      return delay < watchdog_delay;
    }
  };
  class SpotSetter: public Setter {
public:
    SpotSetter() : Setter("spot") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      if(command.get("clear", true)) {
        target.clear();
      }
      std::string what = command.get("what", "grab");
      if(target.spotter == NULL || what == "init") {
        delete target.spotter;
        target.spotter = new SpotEventDetector(command.get("device", 0), command.get("foreground_threshold", 500), command.get("background_count", 20));
      }
      if(what == "register") {
        target.spotter->backgroundRegister();
      } else if(what == "next_move") {
        target.spotter->next(true, command.get("motion_threshold", 10), command.get("watchdog_delay", 0), command.get("stabilization_count", 2), command.get("verbose", false));
      } else if(what == "next_still") {
        target.spotter->next(false, command.get("motion_threshold", 10), command.get("watchdog_delay", 0), command.get("stabilization_count", 2), command.get("verbose", false));
      } else {
        target.spotter->grab();
      }
      target.channels["input"] = target.spotter->input;
      target.parameters["width"] = target.channels["input"].cols;
      target.parameters["height"] = target.channels["input"].rows;
      target.channels["bgmask"] = target.spotter->bgmask;
      wjson::Value& params = target.parameters["spot"];
      params["motion_percentage"] = target.spotter->motionPercentage;
    }
  }
  spotSetter;
}
