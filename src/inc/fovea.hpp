#include "Setter.hpp"

namespace aidecv {
  class FoveaSetter: public Setter {
public:
    FoveaSetter() : Setter("fovea") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      setFovea(target.getAlpha());
    }
    /**
     * @function FoveaSetter::setFovea
     * @memberof aidecv
     * @static
     * @description Implements a basic foveal mask calculation in a alpha channel.
     * - The operation is a multiplicative factor applied on the original image.
     * - The profile is a separable quadratic `|1-x^2|x|1-y^2|` (with |x|=1 on the horizontal borders and |y|=1 on the horizontal borders) factor.
     * @param {Mat} alpha The CV_8UC1 alpha input/output buffer.
     */
    static void setFovea(cv::Mat& alpha)
    {
      unsigned int width = alpha.cols, height = alpha.rows;
      double x, w_i[width], w_j[height];
      for(unsigned int i = 0, i0 = width / 2; i < width; i++) {
        x = (i - (double) i0) / i0, w_i[i] = 1 - x * x;
      }
      for(unsigned int j = 0, j0 = height / 2; j < height; j++) {
        x = (j - (double) j0) / j0, w_j[j] = 1 - x * x;
      }
      unsigned char *alpha_ptr = alpha.ptr < unsigned char > ();
      for(unsigned int j = 0, ij = 0; j < height; j++) {
        for(unsigned int i = 0; i < width; i++, ij++) {
          alpha_ptr[ij] = rint(w_i[i] * w_j[j] * alpha_ptr[ij]);
        }
      }
    }
  }
  foveaSetter;
}
