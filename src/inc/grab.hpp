#include "Setter.hpp"

#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#ifdef ON_RASPPI
#include <raspicam/raspicam_cv.h>
#endif

/* Implementation details:
 * - The implementation is derived from [here](https://github.com/cedricve/raspicam/blob/master/src/raspicam_cv.h) and [there](https://picamera.readthedocs.io/en/release-1.12/fov.html).
 * - PiCamera high resolution acquisition: Since there is a [caveat](https://github.com/cedricve/raspicam/issues/55) using PiCamera high-resolution grabbing with raspicam, we introduced a workaround in [Image.cpp](https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/src/Image.cpp), explicitely calling the `raspistill` line-command.
 * - Raspicam blocking bug: There is a bug using the raspicam/raspicam_cv.h interface, we must release the camera at each call, unless the program blocks, as soon as we perform an I/O operation.
 */

namespace aidecv {
  /* Implements the different grabbing mechanisms. */
  class Grabber {
private:
    // Camera captures devices
    static const unsigned int CAMERA_COUNT = 4;
    cv::VideoCapture captures[CAMERA_COUNT];
#ifdef ON_RASPPI
    raspicam::RaspiCam_Cv camera;
#endif
public:
    ~Grabber() {
      for(unsigned int device = 0; device < CAMERA_COUNT; device++) {
        captures[device].release();
      }
#ifdef ON_RASPPI
      camera.release();
#endif
    }
private:
    /* Grabs via an opencv capture device. */
    void grabDevice(unsigned int device, cv::Mat& img)
    {
      aidesys::alert(CAMERA_COUNT <= device, "illegal-argument", "in aidecv:Image::set/grab, undefined camera device '%d'", device);
      if(!captures[device].isOpened()) {
        captures[device].open(device);
        aidesys::alert(!captures[device].isOpened(), "illegal-argument", "in aidecv::Image::set/grab, unable to open camera device '%d'", device);
      }
      captures[device].read(img);
    }
#ifdef ON_RASPPI
    /* Grabs via a the raspicam capture device. */
    void grabRaspiCam(cv::Mat& img)
    {
      if(!camera.isOpened()) {
        aidesys::alert(!camera.open(), "illegal-state", "in aidecv::Image::set/grab, unable to open the RaspiCam");
      }
      aidesys::alert(!camera.grab(), "illegal-state", "in aidecv::Image::set/grab, RaspiCam image grab fails");
      camera.retrieve(img);
      // This avoids a bug, we must release the camera at each call, unless the program blocks
      camera.release();
    }
#endif
    /* Grabs via an external command. */
    void grabCommand(String command, String file, cv::Mat& img)
    {
      aidesys::system(command);
      img = cv::imread(file.c_str(), cv::IMREAD_UNCHANGED);
    }
public:
    void grab(unsigned int device, cv::Mat& img)
    {
      switch(device) {
#ifdef ON_RASPPI
      case 4:
      {
#if 1
        grabRaspiCam(img);
#else
        // This avoids using raspicam_cv.h
        grabCommand("raspistill -t 1 -w 1280 -h 960 -o /tmp/raspistill.jpg", "/tmp/raspistill.jpg", img);
#endif
        return;
      }
      case 5:
      {
        // This is a workaround to use the RPi camera high resolution
        grabCommand("raspistill -t 1 -o /tmp/raspistill.jpg", "/tmp/raspistill.jpg", img);
        return;
      }
#else
      case 4:
      case 5:
      {
        grabCommand("fmpeg -f video4linux2 -i /dev/video0 -vframes 2 /tmp/img%1d.png", "/tmp/img2.png", img);
        // - grabDevice(0, img);
        aidesys::alert("  illegal-state", "in aidecv:Image::set/grab, on device 4 or 5, not on a RaspPi, using device 0");
        return;
      }
#endif
      default:
      {
        grabDevice(device, img);
        return;
      }
      }
    }
  }
  grabber;

  class GrabSetter: public Setter {
public:
    GrabSetter() : Setter("grab") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      if(command.get("clear", true)) {
        target.clear();
      }
      unsigned int device = command.get("device", 0);
      grabber.grab(device, target.channels["input"]);
      target.parameters["width"] = target.channels["input"].cols;
      target.parameters["height"] = target.channels["input"].rows;
    }
  }
  grabSetter;
}
