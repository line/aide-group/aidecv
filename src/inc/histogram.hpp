#include "Setter.hpp"

namespace aidecv {
  class HistogramSetter: public Setter {
    std::string draw;
    unsigned int smooth;
    bool proba, median, mode;
public:
    HistogramSetter() : Setter("histogram") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      draw = command.get("draw", "");
      smooth = command.get("smooth", 0);
      proba = command.get("proba", false);
      median = command.get("median", false);
      mode = command.get("mode", false);
      bool HSVtodo = true, COtodo = true;
      std::string letters = command.get("what", "BGRHSVTCO");
      for(unsigned int c = 0; c < letters.size(); c++) {
        char letter = letters[c];
        switch(letter) {
        case 'B':
          doHistogram(target, "input", 0, letter, 0);
          break;
        case 'G':
          doHistogram(target, "input", 1, letter, 1);
          break;
        case 'R':
          doHistogram(target, "input", 2, letter, 2);
          break;
        case 'H':
        case 'S':
        case 'V':
        case 'T':
        {
          if(HSVtodo) {
            target.set("hsv");
            HSVtodo = false;
          }
          switch(letter) {
          case 'H':
            doHistogram(target, "hsv", 0, letter, 3);
            break;
          case 'S':
            doHistogram(target, "hsv", 1, letter, 4);
            break;
          case 'V':
            doHistogram(target, "hsv", 2, letter, 5);
            break;
          case 'T':
            doTintHistogram(target);
            break;
          }
          break;
        }
        case 'C':
        case 'O':
        {
          if(COtodo) {
            target.set("{do: edge window: " + command.get("windows", "4") + "}");
            COtodo = false;
          }
          switch(letter) {
          case 'C':
            doHistogram(target, "edge", 0, letter, 6);
            break;
          case 'O':
            doHistogram(target, "edge", 1, letter, 7);
            break;
          }
          break;
        }
        default:
          aidesys::alert(false, "illegal-argument", "in aidecv::Image::set/histogram, bad histogram channel '%c' in the channels set '%s'", letter, letters);
        }
      }
    }
    // Implements the histogram calculation for a given channel of the input with the corresponding letter code and channel index
    void doHistogram(Image& target, String input, unsigned int channel, char letter, unsigned int index)
    {
      // Ref: https://docs.opencv.org/3.4/d8/dbc/tutorial_histogram_calculation.html
      // Histograms ranges, size and color
      static const int sizes[] = {
        256, // Blue
        256, // Green
        256, // Red
        360,// Hue
        100, // Saturation
        100, // Contrast
        100, // Edge magnitude
        360 // Edge orientation
      };
      static const float ranges[][2] = {
        { 0, 256 }, // Blue
        { 0, 256 }, // Green
        { 0, 256 }, // Red
        { 0, 360 }, // Hue
        { 0, 1 }, // Saturation
        { 0, 1 }, // Contrast
        { 0, 1 }, // Edge magnitude
        { 0, 360 } // Edge orientation
      };
      static const bool circulars[] = {
        false, false, false, true, false, false, false, true
      };
      cv::Mat plane;
      cv::extractChannel(target.channels[input], plane, channel);
      const float *hRange = { ranges[index] };
      std::string hname = aidesys::echo("histogram_%c", letter);
      cv::calcHist(&plane, 1, 0, cv::Mat(), target.channels[hname], 1, &sizes[index], &hRange, true, false);
      doHistogram2(target, hname, index, ranges[index], circulars[index]);
    }
    void doHistogram2(Image& target, String hname, unsigned int index, const float range[2], bool circular)
    {
      static cv::Scalar colors[] = {
        cv::Scalar(255, 0, 0), // Blue
        cv::Scalar(0, 255, 0), // Green
        cv::Scalar(0, 0, 255), // Red
        cv::Scalar(255, 0, 255), // Magenta for Hue
        cv::Scalar(255, 255, 0), // Yellow for Saturation
        cv::Scalar(255, 255, 255), // White for Value
        cv::Scalar(128, 128, 128), // Gray for Contrast
        cv::Scalar(255, 128, 128), // Cyan for Orientation
        cv::Scalar(255, 128, 255) // Pink for Tint
      };
      if(smooth > 0) {
        doSmooth(target.channels[hname], smooth, circular);
      }
      if(proba) {
        cv::normalize(target.channels[hname], target.channels[hname], 1.0, 0.0, cv::NORM_L1);
      }
      if(draw != "") {
        doPlot(target.channels[hname], target.channels[hname + "-view"], 200, colors[index], draw == "log");
      }
      if(median) {
        target.parameters["histogram"][hname]["median"] = getMedian(target.channels[hname], range);
      }
      if(mode) {
        target.parameters["histogram"][hname]["mode"] = getMode(target.channels[hname], range);
      }
    }
    void doTintHistogram(Image& target)
    {
      static const float range[2] = { 0, 360 };
      cv::Mat planes[3];
      cv::split(target.channels["hsv"], planes);
      cv::Mat& hist = target.channels["histogram_T"] = cv::Mat(360, 1, CV_32FC1, cv::Scalar(0));
      float *hist_ptr = hist.ptr < float > ();
      const float *hue_ptr = planes[0].ptr < float > (), *sat_ptr = planes[1].ptr < float > ();
      for(int ij = 0; ij < planes[0].cols * planes[0].rows; ij++) {
        unsigned int h = (int) hue_ptr[ij];
        hist_ptr[h < 360 ? h : 359] += sat_ptr[ij];
      }
      doHistogram2(target, "histogram_T", 8, range, true);
    }
    /**
     * @function HistogramSetter::getMode
     * @memberof aidecv
     * @static
     * @description Calculates the histogram median.
     * @param {Mat} input The 1D input array histogram, in `CV_32FC1` format.
     * @return {double} The median value.
     */
    static double getMedian(const cv::Mat& input, const float range[2])
    {
      const float *input_ptr = input.ptr < float > ();
      double c = 0, s, g = (range[1] - range[0]) / input.rows, o = range[0] + g / 2;
      for(int i = 1; i < input.rows; i++) {
        c += input_ptr[i];
      }
      c /= 2;
      for(int i = 0; i < input.rows; i++) {
        s += input_ptr[i];
        if(s >= c) {
          return o + g * i;
        }
      }
      return o + g * input.rows / 2;
    }
    /**
     * @function HistogramSetter::getMode
     * @memberof aidecv
     * @static
     * @description Calculates the histogram mode.
     * @param {Mat} input The 1D input array histogram, in `CV_32FC1` format.
     * @return {double} The mode value.
     */
    static double getMode(const cv::Mat& input, const float range[2])
    {
      const float *input_ptr = input.ptr < float > ();
      float max = 0;
      unsigned int imax = input.rows / 2;
      for(int i = 1; i < input.rows; i++) {
        if(input_ptr[i] > max) {
          max = input_ptr[i], imax = i;
        }
      }
      double g = (range[1] - range[0]) / input.rows, o = range[0] + g / 2;
      return o + g * imax;
    }
    /**
     * @function HistogramSetter::doPlot
     * @memberof aidecv
     * @static
     * @description Plots as a curve an input 1D-array in an BGR image.
     * @param {Mat} input The 1D input array, in `CV_32FC1` format.
     * @param {Mat} output The 2D output `[input.cols x vsize]` image, in `CV_8UC3` format.
     * @param {unit} vsize The output image vertical size.
     * @param {Array} color The curve color as a `cv:Scalar<3>`.
     * @param {bool} in_log If true use logarithm ordinates for the vertical scale.
     */
    static void doPlot(const cv::Mat& input, cv::Mat& output, unsigned int vsize, cv::Scalar& color, bool in_log)
    {
      output = cv::Mat(vsize, input.rows, CV_8UC3, cv::Scalar(0, 0, 0));
      cv::Mat img;
      if(in_log) {
        img = input + 1;
        cv::log(img, img);
      } else {
        img = input;
      }
      cv::normalize(img, img, 0, vsize, cv::NORM_MINMAX, -1, cv::Mat());
      float *img_ptr = img.ptr < float > ();
      for(int i = 1; i < input.rows; i++) {
        cv::line(output,
                 cv::Point(i - 1, vsize - cvRound(img_ptr[i - 1])),
                 cv::Point(i, vsize - cvRound(img_ptr[i])),
                 color, 1);
      }
    }
    /**
     * @function HistogramSetter::doSmooth
     * @memberof aidecv
     * @static
     * @description Smooths a curve using a bi-directional exponential filter.
     * - The img_ptr[0] value is not taken into account to avoid masking effects
     * @param inoutout The curve to smooth.
     * @param window  The smoothing filter window size in pixel, with 90% of the signal to be in a the pixel window.
     * @param circular If the value is circular, smoothing takes it into account.
     *  - The recursive value is initiated considering 2 x window values befores and after the segment bounds.
     */
    static void doSmooth(cv::Mat& inoutput, unsigned int window, bool circular)
    {
      double alpha = pow(0.1, 1.0 / (double) (window < 1 ? 1 : window));
      window = inoutput.rows - 1 < 2 * (int) window ? inoutput.rows - 1 : 2 * window;
      float *img_ptr = inoutput.ptr < float > (), v;
      if(circular) {
        v = 0;
        for(int k = inoutput.rows - window; k < inoutput.rows - 1; k++) {
          v += alpha * (img_ptr[k] - v);
        }
        img_ptr[1] += alpha * (v - img_ptr[1]);
      }
      for(int k = 2; k < inoutput.rows; k++) {
        img_ptr[k] += alpha * (img_ptr[k - 1] - img_ptr[k]);
      }
      if(circular) {
        v = 0;
        for(int k = window; k > 0; k--) {
          v += alpha * (img_ptr[k] - v);
        }
        img_ptr[inoutput.rows - 1] += alpha * (v - img_ptr[inoutput.rows - 1]);
      }
      for(int k = inoutput.rows - 2; k > 0; k--) {
        img_ptr[k] += alpha * (img_ptr[k + 1] - img_ptr[k]);
      }
    }
    /**
     * @function HistogramSetter::getDivergence
     * @memberof aidecv
     * @static
     * @description Calculates the KL-divergence between two histograms.
     * - Implements the [Kullback-Leibler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence).
     * - Both histograms must be normalized before using this function.
     * - Both histograms must have the same size.
     * @param {Mat} data The 1D histogram corresponding to the data distribution.
     * @param {Mat} model The 1D histogram corresponding to the model distribution.
     * @return The KL-divergence, 0 if the same distribution, and below `log_2(hsize)` which is the maximal entropy for an histogram (between 6.5 and 8.5, corresponding to a uniform distribution over 100 to 360 bins).
     */
    static double getDivergence(const cv::Mat& data, const cv::Mat& model)
    {
      aidesys::alert(data.rows != model.rows, "illegal-argument", "in aidecv::Image::set/histogram::getDivergence, histogram sizes '%d' != '%d' differs between data and model", data.rows, model.rows);
      const float *data_ptr = data.ptr < float > (), *model_ptr = model.ptr < float > ();
      double r = 0;
      for(int h = 0; h < data.rows; h++) {
        if(data_ptr[h] > 0 && model_ptr[h] > 0) {
          r += data_ptr[h] * log(data_ptr[h] / model_ptr[h]);
        }
      }
      return r / log(2);
    }
  }
  histogramSetter;
}
