#include "Setter.hpp"

namespace aidecv {
  class IntensitySetter: public Setter {
public:
    IntensitySetter() : Setter("intensity") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      const cv::Mat& img = target.channels["input"];
      unsigned int width = img.cols, height = img.rows;
      const unsigned char *img_ptr = img.ptr < unsigned char > ();

      // Collects the histogram
      unsigned int histo[256];
      {
        for(unsigned int h = 0; h < 256; histo[h++] = 0) {}
        for(unsigned int ijc = 0; ijc < 3 * width * height; ijc++) {
          histo[img_ptr[ijc]]++;
        }
      }
      // Measures the intensity characteristics
      unsigned int hcount = 3 * width * height;
      unsigned int hcumul[256], h25 = 0, h50 = 0, h75 = 0, hmode = 0, hmax = 0, min = 256, max = 0, m1 = 0, m2 = 0;
      double entropy = 0;
      for(unsigned h = 0; h < 256; h++) {
        m1 += histo[h] * h;
        m2 += histo[h] * h * h;
        if(histo[h] > 0) {
          entropy += histo[h] * log(histo[h]);
        }
        if(hmax < histo[h]) {
          hmax = histo[h], hmode = h;
        }
        if(histo[h] > 0 && min == 256) {
          min = h;
        } else if(histo[h] > 0) {
          max = h;
        }
        hcumul[h] = histo[h] + (h > 0 ? hcumul[h - 1] : 0);
        if(4 * hcumul[h] < hcount) {
          h25 = h;
        }
        if(2 * hcumul[h] < hcount) {
          h50 = h;
        }
        if(4 * hcumul[h] < 3 * hcount) {
          h75 = h;
        }
      }
      // Collects the image params
      {
        wjson::Value& params = target.parameters["intensity"];
        params["median"] = h50;
        params["lower-quartile"] = h25;
        params["upper-quartile"] = h75;
        params["minimum"] = min;
        params["maximum"] = max;
        params["mode"] = hmode;
        params["mode-count"] = hmax;
        params["mean"] = hcount == 0 ? 0 : (int) rint(m1 / hcount);
        params["stdev"] = hcount == 0 ? 0 : (int) rint(sqrt((m2 - m1 * m1 / hcount) / hcount));
        params["bin-count"] = (int) rint(pow(hcount, 1.0 / 3.0) * (max - min) / params.get("stdev", 0.0) / 3.49);
        params["entropy"] = hcount == 0 ? 0 : -rint(100 * (entropy / hcount - log(hcount)) / log(2)) / 100;
      }
    }
  }
  intensitySetter;
}
