#include "Setter.hpp"
#include <algorithm>
#include <unordered_map>

namespace aidecv {
  class SegmentSetter: public Setter {
public:
    SegmentSetter() : Setter("segment") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      aidesys::alert(!command.isMember("input"), "illegal-argument", "in aidecv:Image::set/segment, undefined input reference: '" + command.asString() + "'");
      unsigned int size_threshold = command.get("size_threshold", 100);
      cv::Mat source = target.channels[command.at("input").asString()].clone();
      cv::Mat& result = target.channels["segment"];
      wjson::Value& params = target.parameters["segment"];
      params["blobs"] = segment(target.channels["input"], source, result, size_threshold);
      if(command.isMember("draw")) {
        cv::Mat& output = target.channels["output"] = target.channels["input"].clone();
        drawBlobs(params.at("blobs"), result, output, command.get("draw", 0));
      }
    }
    /**
     * @function SegmentSetter::segment
     * @memberof aidecv
     * @static
     * @description Implements the segmentation from a source CV_32SC1 continguous image.
     * - [A] Performs regions chaining.
     *  // [B] Performs regions collection and parameters calculation.
     * @param {Mat} input The input image, a `CV_8u3` image, used for region parameter calculation.
     * @param {Mat} source The input/output mask channel, a `CV_32S1` image,
     * - by contract pixels of the same region have the same index, i.e.:
     *   - Adjacent pixel of the same region have the same non-negative value in the mask.
     *   - Negative values corresponds to unlabeled pixels to be included in the closest indexed region.
     * @param {Mat} result The output region indexing, a `CV_32S1` image,
     * - each pixel value is its region index.
     * @param {uint} size_threshold The minimal pixel count of a blob, default is 100.
     * @return {JSON} A temporary data structure with [region information](commands.html#segment).
     */
    static JSON segment(const cv::Mat& input, cv::Mat& source, cv::Mat& result, unsigned int size_threshold = 100)
    {
      unsigned int width = source.cols, height = source.rows;
      int *source_ptr = source.ptr < int > ();
      result = cv::Mat(cv::Size(width, height), CV_32SC1);
      int *result_ptr = result.ptr < int > ();
      // [A] Regions chaining result_ptr[ij] pixels of the same region have the same index
      {
#define VERBOSE_CHAIN_NO
        // Initializes regions by their index : each pixel is a 1x1 region
        unsigned int count;
        {
          count = 0;
          for(unsigned int j = 0, ij = 0; j < height; j++) {
            for(unsigned int i = 0; i < width; i++, ij++) {
              result_ptr[ij] = ij, count++;
            }
          }
        }
        for(unsigned int step = 0; step < 2; step++) {
#ifdef VERBOSE_CHAIN
          printf("aidecv:Image::set/segment : { step: %d initial-count: %d width x height: %d }\n", step, count, width * height);
#endif
          // Iteratively chains the regions to lower indexes
          {
            static const unsigned int MAX_ITERATION = 10;
            const unsigned int di = 1, dj = width;
            for(unsigned int t = 0, count1 = 0, count2 = 0; t < MAX_ITERATION; t++) {
              for(unsigned int j = 0, ij = 0; j < height; j++) {
                for(unsigned int i = 0; i < width; i++, ij++) {
                  // Chains indexed source_ptr
                  if(source_ptr[ij] >= 0) {
                    // Sets the index to the minimal chain value
                    while(result_ptr[result_ptr[ij]] < result_ptr[ij]) {
                      result_ptr[ij] = result_ptr[result_ptr[ij]];
                    }
                    // Merges horizontaly two adjacent pixels
                    if(0 < i && source_ptr[ij - di] == source_ptr[ij]) {
                      if(result_ptr[ij] > result_ptr[ij - di]) {
                        result_ptr[ij] = result_ptr[result_ptr[ij]] = result_ptr[ij - di];
                      } else if(result_ptr[ij] < result_ptr[ij - di]) {
                        result_ptr[ij - di] = result_ptr[result_ptr[ij - di]] = result_ptr[ij];
                      }
                    }
                    // Merges verticaly two adjacent pixels
                    if(0 < j && source_ptr[ij - dj] == source_ptr[ij]) {
                      if(result_ptr[ij] > result_ptr[ij - dj]) {
                        result_ptr[ij] = result_ptr[result_ptr[ij]] = result_ptr[ij - dj];
                      } else if(result_ptr[ij] < result_ptr[ij - dj]) {
                        result_ptr[ij - dj] = result_ptr[result_ptr[ij - dj]] = result_ptr[ij];
                      }
                    }
                    // Merges in diagonal two adjacent pixels
                    if(0 < i && 0 < j && source_ptr[ij - di - dj] == source_ptr[ij]) {
                      if(result_ptr[ij] > result_ptr[ij - di - dj]) {
                        result_ptr[ij] = result_ptr[result_ptr[ij]] = result_ptr[ij - di - dj];
                      } else if(result_ptr[ij] < result_ptr[ij - di - dj]) {
                        result_ptr[ij - di - dj] = result_ptr[result_ptr[ij - di - dj]] = result_ptr[ij];
                      }
                      count2++;
                    }
                    // Counts root pixels of a region
                    if(result_ptr[ij] == (int) ij) {
                      count1++;
                    }
                  } else if(source_ptr[ij] == -1) {
                    // Integrate unlabbeled pixel in the adjacent region
                    if(0 < i && source_ptr[ij - di] >= 0) {
                      result_ptr[ij] = result_ptr[ij - di], source_ptr[ij] = source_ptr[ij - di];
                    }else if(0 < j && source_ptr[ij - dj] >= 0) {
                      result_ptr[ij] = result_ptr[ij - di], source_ptr[ij] = source_ptr[ij - dj];
                    }else if(i < width - 1 && source_ptr[ij + di] >= 0) {
                      result_ptr[ij] = result_ptr[ij + di], source_ptr[ij] = source_ptr[ij + di];
                    }else if(j < height - 1 && source_ptr[ij + dj] >= 0) {
                      result_ptr[ij] = result_ptr[ij + dj], source_ptr[ij] = source_ptr[ij + dj];
                    }
                  }
                }
              }
#ifdef VERBOSE_CHAIN
              printf("aidecv:Image::set/segment : { t: %d count: %d }\n", t, count1);
#endif
              // Iterates until a stable situation
              if(count1 < count || 0 < count2) {
                count = count1;
              } else {
                break;
              }
            }
#ifdef VERBOSE_CHAIN
            printf("aidecv:Image::set/segment : { final-count: %d }\n", count);
#endif
          }
        }
      }
      // [B] Blob collection and parameters calculation
      {
        static wjson::Value blobs;
        blobs.clear();
        // Blobs pixel index => blob index
        std::unordered_map < unsigned int, unsigned int > indexes;
        for(unsigned int ij = 0; ij < height * width; ij++) {
          if(result_ptr[ij] >= 0) {
            if(indexes.count(result_ptr[ij]) == 0) {
              wjson::Value& blob = blobs[indexes[result_ptr[ij]] = blobs.length()];
              blob["index"] = result_ptr[ij];
              blob["size"] = 1;
            } else {
              wjson::Value& blob = blobs[indexes[result_ptr[ij]]];
              blob["size"] = blob.get("size", 0) + 1;
            }
          }
        }
        // Sorts the blobs
        {
          struct Before {
            static bool before(JSON lhs, JSON rhs)
            {
              return lhs.get("size", 0) == rhs.get("size", 0) ? lhs.get("index", 0) < rhs.get("index", 0) : lhs.get("size", 0) > rhs.get("size", 0);
            }
          };
          blobs.sort(Before::before);
        }
        // Eliminates negigible regions and reindex regions after sort
        {
          indexes.clear();
          for(unsigned int i = 0; i < blobs.length(); i++) {
            if(blobs.at(i).get("size", 0u) >= size_threshold) {
              indexes[blobs.at(i).get("index", 0u)] = i;
            } else {
              blobs.erase(i);
            }
          }
        }
        // Blobs parameters estimation
        {
          // Blobs parameters buffers
          unsigned int count = blobs.length();
          unsigned int top[count], right[count], bottom[count], left[count];
          double m1_x[count], m1_y[count], m2_xx[count], m2_xy[count], m2_yy[count];
          unsigned int blue[count], green[count], red[count];
          for(unsigned int r = 0; r < count; r++) {
            left[r] = -1, top[r] = -1, bottom[r] = 0, right[r] = 0;
            m1_x[r] = m1_y[r] = m2_xx[r] = m2_xy[r] = m2_yy[r] = 0;
            blue[r] = green[r] = red[r] = 0;
          }
          // Collects the blob information from the image segmentation
          const unsigned char *input_ptr = input.ptr < unsigned char > ();
          for(unsigned int j = 0, ij = 0, ijc = 0; j < height; j++) {
            for(unsigned int i = 0; i < width; i++, ij++, ijc += 3) {
              if(result_ptr[ij] >= 0 && indexes.count(result_ptr[ij]) == 1) {
                unsigned int r = indexes.at(result_ptr[ij]);
                left[r] = std::min(left[r], i), right[r] = std::max(right[r], i);
                top[r] = std::min(top[r], j), bottom[r] = std::max(bottom[r], j);
                m1_x[r] += i, m1_y[r] += j, m2_xx[r] += i * i, m2_xy[r] += i * j, m2_yy[r] += j * j;
                blue[r] += input_ptr[ijc], green[r] += input_ptr[ijc + 1], red[r] += input_ptr[ijc + 2];
                result_ptr[ij] = r;
              } else {
                result_ptr[ij] = -1;
                source_ptr[ij] = -1;
              }
            }
          }
          // Updates the blob information
          {
            for(unsigned int r = 0; r < count; r++) {
              wjson::Value& blob = blobs[r];
              unsigned int m0 = blob.get("size", 0);
              blob["index"] = r;
              blob["x"] = rint(m1_x[r] / m0);
              blob["y"] = rint(m1_y[r] / m0);
              m2_xx[r] = (m2_xx[r] - m1_x[r] * m1_x[r] / m0) / m0, m2_xy[r] = (m2_xy[r] - m1_x[r] * m1_y[r] / m0) / m0, m2_yy[r] = (m2_yy[r] - m1_y[r] * m1_y[r] / m0) / m0;
              double mu_s = m2_xx[r] + m2_yy[r], mu_d = m2_yy[r] - m2_xx[r], mu_q = sqrt(mu_d * mu_d + m2_xy[r] * m2_xy[r]);

              blob["a"] = rint(180.0 / M_PI * (0.5 * atan2(-2.0 * m2_xy[r], mu_d)));
              blob["L"] = rint(sqrt(mu_s + mu_q));
              blob["l"] = rint(sqrt(mu_s - mu_q));
              blob["e"] = rint(mu_s == 0 ? 0 : 100.0 * mu_q / mu_s);
              blob["top"] = top[r];
              blob["right"] = right[r];
              blob["bottom"] = bottom[r];
              blob["left"] = left[r];
              blob["blue"] = blue[r] / m0;
              blob["green"] = green[r] / m0;
              blob["red"] = red[r] / m0;
            }
          }
        }
        return blobs;
      }
    }
    void drawBlobs(JSON blobs, const cv::Mat& result, cv::Mat& output, unsigned int mode)
    {
      // Draws the blobs pixels
      if(mode & 0x30) {
        unsigned int width = output.cols, height = output.rows;
        unsigned char *output_ptr = output.ptr < unsigned char > ();
        const int *result_ptr = result.ptr < int > ();
        static const unsigned int colors[8][3] = { { 64, 64, 64 }, { 21, 21, 81 }, { 0, 0, 128 }, { 0, 70, 128 }, { 32, 64, 64 }, { 0, 128, 0 }, { 128, 0, 0 }, { 64, 0, 64 } };
        for(unsigned int ij = 0, ijc = 0; ij < width * height; ij++, ijc += 3) {
          if(result_ptr[ij] >= 0) {
            JSON blob = blobs.at(result_ptr[ij]);
            unsigned int c = blob.get("index", 0) % 8;
            output_ptr[ijc] = mode & 0x10 ? blob.get("blue", 0) : colors[c][0];
            output_ptr[ijc + 1] = mode & 0x10 ? blob.get("green", 0) : colors[c][1];
            output_ptr[ijc + 2] = mode & 0x10 ? blob.get("red", 0) : colors[c][2];
          } else {
            for(unsigned int c = 0; c < 3; c++) {
              output_ptr[ijc + c] /= 4;
            }
          }
        }
      }
      // Draws each blob characteristics
      {
        for(unsigned int i = 0; i < blobs.length(); i++) {
          drawBlob(output, blobs.at(i), mode);
        }
      }
    }
    void drawBlob(cv::Mat& output, JSON blob, unsigned int mode) const
    {
      cv::Scalar color = cv::Scalar(255, 255, 255);
      if(mode & 0x08) {
        cv::ellipse(output, cv::Point(blob.get("x", 0), blob.get("y", 0)), cv::Size(blob.get("L", 0), blob.get("l", 0)), 90 + blob.get("a", 0), 0, 360, color, 2);
      }
      if(mode & 0x04) {
        cv::rectangle(output, cv::Point(blob.get("left", 0), blob.get("top", 0)), cv::Point(blob.get("right", 0), blob.get("bottom", 0)), color, 2);
      }
      if(mode & 0x02) {
        unsigned int l = 0.5 * sqrt(blob.get("size", 0));
        cv::line(output, cv::Point(blob.get("x", 0) + l, blob.get("y", 0)), cv::Point(blob.get("x", 0) - l, blob.get("y", 0)), color, 2);
        cv::line(output, cv::Point(blob.get("x", 0), blob.get("y", 0) + l), cv::Point(blob.get("x", 0), blob.get("y", 0) - l), color, 2);
      }
      if(mode & 0x01) {
        int baseline[2];
        std::string name = blob.get("name", blob.get("index", ""));
        cv::Size s = cv::getTextSize(name, 0, 1, 2, baseline);
        cv::putText(output, name, cv::Point(10 + blob.get("x", 0) - s.width / 2, blob.get("y", 0)), 0, 1, color, 2);
      }
    }
  }
  segmentSetter;
}
