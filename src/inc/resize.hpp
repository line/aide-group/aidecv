#include "Setter.hpp"

#include <opencv2/imgproc.hpp>

namespace aidecv {
  class ResizeSetter: public Setter {
public:
    ResizeSetter() : Setter("resize") {}
    void set(Image& target, JSON command, const Image& source)
    {
      int width, height;
      target.parameters["width"] = width = command.get("width", source.parameters.get("width", target.parameters.get("width", 0)));
      target.parameters["height"] = height = command.get("height", source.parameters.get("height", target.parameters.get("height", 0)));
      aidesys::alert(width <= 0 || height <= 0, "illegal-argument", " aidecv::Image::set/resize, spurious sizes [%d, %d]", width, height);
      for(auto channel : target.channels) {
        cv::resize(channel.second, channel.second, cv::Size(width, height), 0, 0,
                   channel.second.type() == CV_32SC1 ? cv::INTER_NEAREST :
                   channel.second.cols < width && channel.second.rows < height ? cv::INTER_CUBIC :
                   cv::INTER_AREA);
        target.channels[channel.first] = channel.second;
      }
    }
  }
  resizeSetter;
}
