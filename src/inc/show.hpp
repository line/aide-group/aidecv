#include "Setter.hpp"

#include <unistd.h>

namespace aidecv {
  class ShowSetter: public Setter {
public:
    ShowSetter() : Setter("show") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      // Defines the channels to show
      wjson::Value channels = command.at("channels");
      {
        if(target.parameters["show"].isMember("channel")) {
          channels[channels.length()] = target.parameters["show"]["channel"];
        }
        if(channels.length() == 0) {
          channels[channels.length()] = "input";
        }
      }
      {
        // Defines the image display name prefix
        static unsigned int index = 0;
        std::string prefix =
          target.parameters.at("show").isMember("name") ? "name:" + target.parameters.at("show").get("name", "") :
          target.parameters.at("load").isMember("file") ? "file:" + target.parameters.at("load").get("file", "") :
          target.parameters.at("grab").isMember("device") ? "device:" + target.parameters.at("grab").get("device", "") :
          target.parameters.at("cache").isMember("name") ? "cache:" + target.parameters.at("cache").get("name", "") :
          aidesys::echo("index:%d", ++index);
        // Loops on all channels
        for(unsigned int i = 0; i < channels.length(); i++) {
          std::string name = channels.get(i, "");
          auto it = target.channels.find(name);
          aidesys::alert(it == target.channels.end(), "illegal-argument", "in aidecv::Image::set/show, undefined channel '" + name + "'");
          std::string wname = prefix + "::" + name;
          opencvwindow::show(wname, mat2mat(it->second));
        }
      }
    }
  }
  showSetter;
}
