#include "Setter.hpp"

#include <opencv2/imgproc/imgproc.hpp>

namespace aidecv {
  class HsvSetter: public Setter {
public:
    HsvSetter() : Setter("hsv") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      target.set("bgr");
      cv::cvtColor(target.channels["bgr"], target.channels["hsv"], cv::COLOR_BGR2HSV);
    }
  }
  hsvSetter;
}
