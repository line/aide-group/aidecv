#include "Setter.hpp"

namespace aidecv {
  class RectifySetter: public Setter {
public:
    RectifySetter() : Setter("rectify") {}
    void set(Image& target, JSON command, const Image& source)
    {
      if(command.get("clear", true)) {
        target.clear();
      }
      unsigned int source_width = source.parameters.get("width", 0);
      unsigned int source_height = source.parameters.get("height", 0);
      unsigned int width = command.get("width", source_width);
      unsigned int height = command.get("height", source_height);
      // Defines the final quadrilateral
      cv::Point2f dst[4] = {
        cv::Point2f(0, 0),
        cv::Point2f(width, 0),
        cv::Point2f(width, height),
        cv::Point2f(0, height)
      };
      // Defines the initial quadrilateral
      cv::Point2f *src, src_[5] = {
        cv::Point2f(command.get("left_x", 0), command.get("left_y", source_height - 1)),
        cv::Point2f(command.get("top_x", 0), command.get("top_y", 0.0)),
        cv::Point2f(command.get("right_x", source_width - 1), command.get("right_y", 0)),
        cv::Point2f(command.get("bottom_x", source_width - 1), command.get("bottom_y", source_height - 1)),
        cv::Point2f(command.get("left_x", 0), command.get("left_y", source_height - 1))
      };
      // Checks if the left or bottom point is closer to the origin
      src = (int) command.get("left_y", height - 1) < command.get("top_x", 0) ? src_ : &src_[1];
      // Calculates the rectification matrix and generates the result
      cv::Mat H = cv::getPerspectiveTransform(src, dst);
      // Performs the perspective transform
      cv::Mat result;
      cv::warpPerspective(source.channels.at("input"), result, H, cv::Size(width, height), cv::INTER_LINEAR, cv::BORDER_CONSTANT, 0);
      target.channels["input"] = result;
      target.parameters["width"] = result.cols;
      target.parameters["height"] = result.rows;
    }
  }
  rectifySetter;
}
