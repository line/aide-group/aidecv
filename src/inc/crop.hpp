#include "Setter.hpp"

namespace aidecv {
  class CropSetter: public Setter {
public:
    CropSetter() : Setter("crop") {}
    void set(Image& target, JSON command, const Image& source)
    {
      aidesys::alert(&target == &source, "illegal-argument", "in aidecv::Image::set/crop target and source can not be the same");
      if(command.get("clear", true)) {
        target.clear();
      }
      unsigned int width = source.parameters.get("width", 0);
      unsigned int height = source.parameters.get("height", 0);
      unsigned int top = command.get("top", 0);
      unsigned int left = command.get("left", 0);
      unsigned int right = std::max(left, std::min(command.get("right", width), width));
      unsigned int bottom = std::max(top, std::min(command.get("bottom", height), height));
      cv::Rect roi(left, top, (right > width ? width : right) - left, (bottom > height ? height : bottom) - top);
      target.channels["input"] = source.channels.at("input")(roi).clone();
      target.parameters["width"] = target.channels["input"].cols;
      target.parameters["height"] = target.channels["input"].rows;
    }
  }
  cropSetter;
}
