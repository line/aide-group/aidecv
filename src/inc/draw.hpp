#include "Setter.hpp"

namespace aidecv {
  class DrawSetter: public Setter {
public:
    DrawSetter() : Setter("draw") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      if(target.channels.find("draw") == target.channels.end()) {
        target.channels["draw"] = target.channels["input"].clone();
      }
      draw(target.channels["draw"], command.at("what"));
    }
    /**
     * @function DrawSetter::draw
     * @memberof aidecv
     * @static
     * @description Implements 2D geometric drawing on an image.
     * @param {Mat} output The output image to draw on.
     * @param {JSON} what The [data structure](./commands.html#draw) containing what is to drawn.
     */
    static void draw(cv::Mat& output, JSON what)
    {
      aidesys::alert(!what.isArray(), "illegal-argument", "in aidecv::Image::set/draw, nothing to be drawn :'" + what.asString());
      for(unsigned int i = 0; i < what.length(); i++) {
        drawOnce(output, what.at(i));
      }
    }
    // Draws one element
    static void drawOnce(cv::Mat& output, JSON draw)
    {
      aidesys::alert(!(draw.isRecord() && draw.isMember("do")), "illegal-argument", "in aidecv::Image::set/draw, spurious draw command :'" + draw.asString() + "'");
      std::string tobedone = draw.get("do", "");
      cv::Scalar color = getColor((draw.get("color", "w"))[0]);
      if(tobedone == "text") {
        aidesys::alert(!(draw.isMember("x") && draw.isMember("y") && draw.isMember("text")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command :'" + draw.asString());
        int baseline[2];
        std::string text = draw.get("text", "");
        cv::Size s = cv::getTextSize(text, 0, 1, 2, baseline);
        cv::putText(output, text, cv::Point(10 + draw.get("x", 0) - s.width / 2, draw.get("y", 0)), 0, 1, color, 2);
      } else if(tobedone == "cross") {
        aidesys::alert(!(draw.isMember("x") && draw.isMember("y") && draw.isMember("l")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        unsigned int l = draw.get("l", 0) / 2;
        cv::line(output, cv::Point(draw.get("x", 0) + l, draw.get("y", 0)), cv::Point(draw.get("x", 0) - l, draw.get("y", 0)), color, 2);
        cv::line(output, cv::Point(draw.get("x", 0), draw.get("y", 0) + l), cv::Point(draw.get("x", 0), draw.get("y", 0) - l), color, 2);
      } else if(tobedone == "line") {
        aidesys::alert(!(draw.isMember("x1") && draw.isMember("y1") && draw.isMember("x2") && draw.isMember("y2")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        cv::line(output, cv::Point(draw.get("x1", 0), draw.get("y1", 0)), cv::Point(draw.get("x2", 0), draw.get("y2", 0)), color, 2);
      } else if(tobedone == "rectangle" || tobedone == "crop") {
        aidesys::alert(!(draw.isMember("left") && draw.isMember("top") && draw.isMember("right") && draw.isMember("bottom")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        cv::rectangle(output, cv::Point(draw.get("left", 0), draw.get("top", 0)), cv::Point(draw.get("right", 0), draw.get("bottom", 0)), color, 2);
      } else if(tobedone == "quadrilateral" || tobedone == "rectify") {
        aidesys::alert(!(draw.isMember("top_x") && draw.isMember("top_y") && draw.isMember("right_x") && draw.isMember("right_y") && draw.isMember("bottom_x") && draw.isMember("bottom_y") && draw.isMember("left_x") && draw.isMember("left_y")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        cv::line(output, cv::Point(draw.get("top_x", 0), draw.get("top_y", 0)), cv::Point(draw.get("right_x", 0), draw.get("right_y", 0)), color, 2);
        cv::line(output, cv::Point(draw.get("bottom_x", 0), draw.get("bottom_y", 0)), cv::Point(draw.get("right_x", 0), draw.get("right_y", 0)), color, 2);
        cv::line(output, cv::Point(draw.get("bottom_x", 0), draw.get("bottom_y", 0)), cv::Point(draw.get("left_x", 0), draw.get("left_y", 0)), color, 2);
        cv::line(output, cv::Point(draw.get("top_x", 0), draw.get("top_y", 0)), cv::Point(draw.get("left_x", 0), draw.get("left_y", 0)), color, 2);
      } else if(tobedone == "circle") {
        aidesys::alert(!(draw.isMember("x") && draw.isMember("y") && draw.isMember("r")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        cv::ellipse(output, cv::Point(draw.get("x", 0), draw.get("y", 0)), cv::Size(draw.get("r", 0), draw.get("r", 0)), 0, 0, 360, color, 2);
      } else if(tobedone == "ellipse") {
        aidesys::alert(!(draw.isMember("x") && draw.isMember("y") && draw.isMember("L") && draw.isMember("l") && draw.isMember("a")), "illegal-argument", "in aidecv::Image::set/draw, bad draw command: '" + draw.asString());
        cv::ellipse(output, cv::Point(draw.get("x", 0), draw.get("y", 0)), cv::Size(draw.get("L", 0), draw.get("l", 0)), 90 + draw.get("a", 0), 0, 360, color, 2);
      }
    }
    // A char 2 color converter
    static cv::Scalar getColor(char c)
    {
      switch(c) {
      case 'r':
        return cv::Scalar(0, 0, 255);
      case 'g':
        return cv::Scalar(0, 255, 0);
      case 'b':
        return cv::Scalar(255, 0, 0);
      case 'y':
        return cv::Scalar(255, 255, 0);
      case 'c':
        return cv::Scalar(0, 255, 255);
      case 'm':
        return cv::Scalar(255, 0, 255);
      case '0':
        return cv::Scalar(0, 0, 0);
      default:
      case 'w':
        return cv::Scalar(255, 255, 255);
      }
    }
  }
  drawSetter;
}
