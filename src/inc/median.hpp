#include "Setter.hpp"

namespace aidecv {
  class MedianSetter: public Setter {
public:
    MedianSetter() : Setter("median") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      cv::medianBlur(target.channels["input"], target.channels["input"], command.get("window", 3));
    }
  }
  medianSetter;
}
