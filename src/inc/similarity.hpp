#include "Setter.hpp"
#include "Similarity.hpp"

namespace aidecv {
  class SimilaritySetter: public Setter {
public:
    SimilaritySetter() : Setter("similarity") {}
    void set(Image& target, JSON command, const Image& source)
    {
      std::string similarity = command.get("similarity", "correlation");
      const_cast < Image & > (source).getAlpha();
      // Calls the similirity calculation given the parameters
      {
        std::map < std::string, Similarity * >& similarities = Similarity::getSimilarities();
        auto it = similarities.find(similarity);
        aidesys::alert(it == similarities.end(), "illegal-argument", "in aidecv::Image::set/similarity, undefined similarity '" + similarity + "'");
        wjson::Value& params = target.parameters["similarity"];
        params["value"] = it->second->get(target, source);
      }
    }
  }
  similaritySetter;

  // Implements the correlation similiraity
  class CorrelationSimilarity: public Similarity {
public:
    CorrelationSimilarity() : Similarity("correlation") {}
    double get(Image& target, const Image& source)
    {
      return getCorrelation(target.channels.at("input"), source.channels.at("input"), source.channels.at("alpha"));
    }
    /* Implements the normalize correlation calculation.
     * @param {cv::Mat} img The computation input image.
     * @param {cv::Mat} src The computation reference.
     * @param {cv::Mat} alpha An alpha mask to take or not a given pixel into account..
     */
    static double getCorrelation(const cv::Mat& img, const cv::Mat& src, const cv::Mat& alpha)
    {
      unsigned int width = img.cols, height = img.rows;
      aidesys::alert(img.cols != src.cols || img.rows != src.rows, "illegal-argument", "in aidecv::Image::set/similarity, incompatible image sizes between source (%dx%d) and target (%dx%d)", src.cols, src.rows, img.cols, img.rows);
      const unsigned char *img_ptr = img.ptr < unsigned char > (), *src_ptr = src.ptr < unsigned char > (), *alpha_ptr = alpha.ptr < unsigned char > ();
      //
      double m_00 = 0, m_01 = 0, m_10 = 0, m_02 = 0, m_20 = 0, m_11 = 0;
      for(unsigned int j = 0, ij = 0, ijc = 0; j < height; j++) {
        for(unsigned int i = 0; i < width; i++, ij++) {
          double w = alpha_ptr[ij];
          for(unsigned int c = 0; c < 3; c++, ijc++) {
            if(img_ptr[ijc] < 255) { // Pixels with value at 255 are not taken into account
              m_00 += w, m_10 += w * img_ptr[ijc], m_01 += w * src_ptr[ijc], m_20 += w * img_ptr[ijc] * img_ptr[ijc], m_02 += w * src_ptr[ijc] * src_ptr[ijc], m_11 += w * img_ptr[ijc] * src_ptr[ijc];
            }
          }
        }
      }
      double d = (m_00 * m_20 - m_10 * m_10) * (m_00 * m_02 - m_01 * m_01);
      return 2 * (1 - (d > 0 ? (m_00 * m_11 - m_10 * m_01) / sqrt(d) : 0));
    }
  }
  correlationSimilarity;

  // Implements a hue similarity
  class HueSimilarity: public Similarity {
public:
    HueSimilarity() : Similarity("hue") {}
    double get(Image& target, const Image& source)
    {
      const_cast < Image & > (source).set("{do: histogram what: H smooth: 5 mode: true }");
      target.set("{do: histogram what: H smooth: 5 mode: true }");
      double v_src = source.parameters.at("histogram").at("histogram_H").get("mode", 0.0);
      double v_img = target.parameters.at("histogram").at("histogram_H").get("mode", 0.0);
      double d0 = fabs(v_src - v_img), d1 = fabs(v_src - v_img + 360), d2 = fabs(v_src - v_img - 360);
      return d0 < d1 ? (d0 < d2 ? d0 : d2) : (d1 < d2 ? d1 : d2);
    }
  }
  hueSimilarity;

  // Implements a binary similarity
  class BinarySimilarity: public Similarity {
public:
    BinarySimilarity() : Similarity("binary") {}
    double get(Image& target, const Image& source)
    {
      return getCorrelation(target.channels.at("input"), source.channels.at("input"), source.channels.at("alpha"));
    }
    /* Implements the binary correlation calculation.
     * @param {cv::Mat} img The computation input image.
     * @param {cv::Mat} src The computation reference.
     * @param {cv::Mat} alpha An alpha mask to take or not a given pixel into account..
     */
    static double getCorrelation(const cv::Mat& img, const cv::Mat& src, const cv::Mat& alpha)
    {
      aidesys::alert(img.cols != src.cols || img.rows != src.rows, "illegal-argument", "in aidecv::Image::set/similarity, incompatible image sizes between source (%dx%d) and target (%dx%d)", src.cols, src.rows, img.cols, img.rows);
      unsigned int width = img.cols, height = img.rows;
      cv::Mat imgb = bgr2bin(img);
      const unsigned char *imgb_ptr = imgb.ptr < unsigned char > (), *src_ptr = src.ptr < unsigned char > (), *alpha_ptr = alpha.ptr < unsigned char > ();
      double result = 256;
      for(int v = -(int) height / 4; v < (int) height / 4; v++) {
        for(int u = -(int) width / 4; u < (int) width / 4; u++) {
          double result_n = 0, result_d = 0;
          for(unsigned int j = 0, ij = 0, ijc = 0; j < height; j++) {
            for(unsigned int i = 0; i < width; i++, ij++) {
              if(0 <= j + v && j + v < height && 0 <= i + u && i + u < width) {
                int w = alpha_ptr[ij], v0 = imgb_ptr[i + u + width * (j + v)], v1 = src_ptr[ijc];
                result_d += w;
                result_n += w * std::abs(v0 - v1);
              }
            }
          }
          if(result_n > 0 && result_d * result > result_n) {
            result = result_n / result_d;
          }
        }
      }
      result /= 256;
      return result;
    }
  }
  binarySimilarity;

  std::map < std::string, Similarity * > &Similarity::getSimilarities() {
    static std::map < std::string, Similarity * > similarities;
    return similarities;
  }
  Similarity::Similarity(String what)
  {
    getSimilarities()[what] = this;
  }
  double Similarity::get(Image& target, const Image& source)
  {
    aidesys::alert("illegal-state", "in aidecv::Image::Similarity::get, not implemented");
    return NAN;
  }
}
