#include "Setter.hpp"
#include "file.hpp"

#include <opencv2/imgcodecs/imgcodecs.hpp>

namespace aidecv {
  class SaveSetter: public Setter {
    unsigned int index = 0;
public:
    SaveSetter() : Setter("save") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      std::string path = command.get("path", aidesys::echo("/tmp/aidecvImage-%d", ++index));
      // Saves the parameters with all channels description and thus create the directory `path`
      for(auto channel : target.channels) {
        target.parameters["channels"][channel.first] = mat2json(channel.second, false);
      }
      aidesys::save(path + "/parameters.json", target.parameters.asString(true));
      // Saves all cv::Mat images
      for(auto channel : target.channels) {
        std::string basename = path + "/" + channel.first;
        const cv::Mat& img = channel.second;
        switch(img.type()) {
        case CV_8UC1:
        case CV_8UC3:
          cv::imwrite(basename + ".png", img);
          break;
        case CV_32FC2:
          cv::imwrite(basename + ".exr", mat2mat(img));
          break;
        case CV_32SC1:
        {
          cv::imwrite(basename + ".exr", mat2mat(img));
          break;
        }
        default:
          cv::imwrite(basename + ".exr", img);
          break;
        }
      }
    }
  }
  saveSetter;
}
