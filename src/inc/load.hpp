#include "Setter.hpp"
#include "file.hpp"

#include <opencv2/imgcodecs/imgcodecs.hpp>
#ifndef CV_LOAD_IMAGE_COLOR
#define CV_LOAD_IMAGE_COLOR cv::IMREAD_COLOR
#endif

namespace aidecv {
  class LoadSetter: public Setter {
public:
    LoadSetter() : Setter("load") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      if(command.get("clear", true)) {
        target.clear();
      }
      std::string file = command.get("file", "");
      std::string alpha = command.get("alpha", "");
      aidesys::alert(!aidesys::exists(file), "illegal-argument", "in aidecv::Image::set/load, the file '" + file + "' does not exist");
#if 1
      cv::Mat& img = target.channels["input"] = cv::imread(file.c_str(), cv::IMREAD_UNCHANGED);
      if(img.channels() == 4) {
        cv::Mat channels[4];
        cv::split(img, channels);
        cv::merge(channels, 3, target.channels["input"]);
        if(alpha != "false") {
          channels[3].convertTo(target.channels["alpha"], CV_8UC1);
        }
      } else {
        aidesys::alert(img.channels() != 3, "illegal-argument", "in aidecv::Image::set/load, the file '" + file + "' has a spurious '%d' number of channels", img.channels());
        if(alpha == "true") {
          target.getAlpha();
        }
      }
#else
      // Only reads the BGR channels
      target.channels["input"] = cv::imread(file.c_str(), CV_LOAD_IMAGE_COLOR);
#endif
      target.parameters["width"] = target.channels["input"].cols;
      target.parameters["height"] = target.channels["input"].rows;
    }
  }
  loadSetter;
}
