#include "Setter.hpp"

#include <opencv2/xphoto/white_balance.hpp>

namespace aidecv {
  class BalanceSetter: public Setter {
public:
    BalanceSetter() : Setter("balance") {}
    void set(Image& target, JSON command, const Image& empty)
    {
      std::string balance = command.get("balance", "simple");
      if(balance == "grayworld") {
        auto wb = cv::xphoto::createGrayworldWB();
        wb->setSaturationThreshold(command.get("saturation", 0.9));
        wb->balanceWhite(target.channels["input"], target.channels["input"]);
      } else if(balance == "learned") {
        auto wb = cv::xphoto::createLearningBasedWB();
        wb->setSaturationThreshold(command.get("saturation", 0.9));
        wb->balanceWhite(target.channels["input"], target.channels["input"]);
      } else {
        auto wb = cv::xphoto::createSimpleWB();
        wb->setP(command.get("saturation", 2.0));
        wb->balanceWhite(target.channels["input"], target.channels["input"]);
      }
      if(command.get("equalize", false)) {
        cv::Mat bgr[3];
        cv::split(target.channels["input"], bgr);
        for(unsigned int i = 0; i < 3; i++) {
          cv::equalizeHist(bgr[i], bgr[i]);
        }
        cv::merge(bgr, 3, target.channels["input"]);
      }
    }
  }
  balanceSetter;
}
