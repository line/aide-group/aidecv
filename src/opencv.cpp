#include "opencv.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace aidecv {
  // Returns a cv::Mat temporary matrix that can be saved or shown.
  const cv::Mat& mat2mat(const cv::Mat& img)
  {
    static cv::Mat result;
    switch(img.type()) {
    case CV_32FC2:
    {     // Note: only 1, 3 or 4 channels image can be saved
      cv::Mat planes[3];
      cv::split(img, planes);
      planes[2] = cv::Mat(cv::Size(img.cols, img.rows), CV_32FC1, cv::Scalar(0));
      cv::merge(planes, 3, result);
      return result;
    }
    case CV_32SC1:
    {
      img.convertTo(result, CV_32FC1);
      return result;
    }
    default:
      return img;
    }
  }
  // Returns the cv::Mat characteristics as a string, with optionally pixels mean and standard-deviation.
  JSON mat2json(const cv::Mat& img, bool meanstdev)
  {
    std::string type;
    switch(img.type() & CV_MAT_DEPTH_MASK) {
    case CV_8U:
      type = "8U";
      break;
    case CV_8S:
      type = "8S";
      break;
    case CV_16U:
      type = "16U";
      break;
    case CV_16S:
      type = "16S";
      break;
    case CV_32S:
      type = "32S";
      break;
    case CV_32F:
      type = "32F";
      break;
    case CV_64F:
      type = "64F";
      break;
    default:
      type = "user-defined";
      break;
    }
    unsigned int depth = 1 + (img.type() >> CV_CN_SHIFT);
    static wjson::Value result;
    result.clear();
    result["cols"] = img.cols;
    result["rows"] = img.rows;
    result["depth"] = depth;
    result["type"] = type;
    if(!img.isContinuous()) {
      result["continuous"] = false;
    }
    if(meanstdev) {
      cv::Mat mean, stdev;
      cv::meanStdDev(img, mean, stdev);
      for(unsigned int c = 0; c < depth; c++) {
        result["mean"][c] = mean.at < double > (c);
        result["stdev"][c] = stdev.at < double > (c);
      }
    }
    return result;
  }
  // Returns the image data in textual form
  std::string mat2txt(const cv::Mat& img)
  {
    std::ostringstream stream;
    stream << img;
    return stream.str();
  }
  // Converts automatically an BGR image to a binary one
  const cv::Mat& bgr2bin(const cv::Mat& img)
  {
    static cv::Mat result;
    cv::cvtColor(img, result, cv::COLOR_BGR2GRAY);
    cv::threshold(result, result, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    return result;
  }
  // Shows an image in a window, managing the window registration
  void opencvwindow::show(String name, const cv::Mat& img)
  {
    cv::namedWindow(name, cv::WINDOW_AUTOSIZE);
    getWindowNames().push_back(name);
    cv::imshow(name, img);
  }
  // Waits untill all windows opened by the show command are closed.
  void opencvwindow::waitUntilWindowsClosed()
  {
    std::vector < std::string >& windowNames = getWindowNames();
    for(bool wait = true; wait; cv::waitKey(500)) {
      wait = false;
      for(auto it = windowNames.begin(); it != windowNames.end(); it++) {
        if(cv::getWindowProperty(*it, cv::WND_PROP_VISIBLE) >= 1.0) {
          wait = true;
        }
      }
    }
  }
  std::vector < std::string > &opencvwindow::getWindowNames() {
    static std::vector < std::string > windowNames;
    return windowNames;
  }
}
