/**
 * @class commands
 * @description Setter command parameters.
 *
 * - [General mechanism](#introduction)
 * - [Available commands](#commands)
 *   - [grab](#grab) Grabs an image from a source device.
 *   - [spot](#spot) Grabs an image after, spoting, detecting a motion event.
 *   - [load](#load) Loads an image from a source file.
 *   - [save](#save) Saves an image in files.
 *   - [cache](#cache) Caches an image in memory.
 *   - [copy](#copy) Copies an image from a source image.
 *   - [crop](#crop) Crops a rectangle image from a source image.
 *   - [rectify](#rectify) Crops a quadrilateral from a source image.
 *   - [intensity](#intensity) Calculates image intensity statistics.
 *   - [histogram](#histogram) Calculates image histograms.
 *   - [similarity](#similarity) Computes the similarity with respect to a source image.
 *   - [balance](#balance) Balances the image intensity.
 *   - [median](#median) Smooths the image intensity.
 *   - [fovea](#fovea) Defines a foveal mask of transparency to limit image border influence.
 *   - [resize](#resize) Resizes an image.
 *   - [bgr](#bgr) Converts the Blue-Green-Red input in float format.
 *   - [hsv](#hsv) Computes the Hue-Saturation-Value input in float format.
 *   - [smooth](#smooth) Smooths the input using an exponential filter in float format.
 *   - [edge](#edge) Computes the image edges in float format.
 *   - [subtract](#subtract) Subtracts a background from this image.
 *   - [blob](#blob) Thresholds a colored blob in this image.
 *   - [segment](#segment) Segments and parse blobs in this image.
 *   - [match](#match) Matches the image against source·s images.
 *   - [draw](#draw) Draws elements on a copy of this image input.
 *   - [show](#show) Displays channel·s in popup window·s.
 *
 * - [Caution about bug and caveat](#bugs)
 *
 * ## General mechanism  <a name="introduction"></a>
 *
 * - Each command is defined by:
 *   - A name given in the `do` field.
 *   - A set of input parameters, stored in the Image parameters under the command name on output.
 *
 * _Remarks_:
 * - Each command is designed to be applied once on an Image, otherwise image copied is to advised.
 * - Other mechanisms can be implemented via the [Setter](https://gitlab.inria.fr/line/aide-group/aidecv/-/blob/master/src/Setter.hpp) hook.
 *
 * ## Available commands  <a name="commands"></a>
 *
 * - <a name="grab"></a> __[grab]__ `{ do: grad device: $device_index }` or `$device_index`.
 *   - Grabs a BGR `CV_32UC3` image from an input device.
 *   - _Input parameter_:
 *     - The `device` index, it can simply be given as a string, without JSON construct.
 *       - 0,1,2,3 corresponds to the _/dev/video$device-number_ device.
 *       - 4  On a RPi, it corresponds to the 1280x960 resolution PiCamera.
 *       - 5  On a RPi, it corresponds to the full-resolution PiCamera using the _raspistill_ utility.
 *     - `clear`: if not false image channels and parameters are beforehand cleared.
 *   - _Output channel_: The raw `input` image, in `CV_8UC3` format.
 *   - _Output parameters_: Image `width` and `height`.
 *
 * - <a name="spot"></a> __[spot]__ `{ do: grad device: $device_index }` or `$device_index`.
 *   - Spots a BGR `CV_32UC3` image from an input device, after detecting a motion.
 *   - _Input parameter_:
 *     - The input command `what`, either:
 *       - `init` : to init the mechanism using the parameters given next. Algorithm parameters:
 *         - `device` the input device index to grab:
 *         - `foreground_threshold` The foreground threshold detection level, default is 500.
 *         - `background_count` The number of frame to average for the background, default is 20.
 *       - `register` : to register the background before spotting.
 *       - `grab` : to grab an image an evaluate the percentage `motion-ratio` percentage of moving pixels.
 *       - `next_move` or `next_still` : to wait (thus blocking the program) until a visual event: Detects the percentage `motion-ratio` percentage of moving pixels with respect to a threshold. If `next-move` detects when above the threshold, if `next-still` detects when under the threshold.
 *         - `motion_threshold` The motion threshold in percentage, default is 10.
 *         - `watchdog_delay` The maximal delay to wait for an event, in milli-seconds, default is 0, i.e., waiting forever.
 *         - `stabilization_count` Number of frames before the event is considered as stable and not spurious, default is 2.
 *         - `verbose` If true prints a trace during the detection delay, default is false.
 *     - `clear`: if not false image channels and parameters are beforehand cleared.
 *   - _Output channels_:
 *      - The raw `input` image, in `CV_8UC3` format.
 *      - The background mask `bgmask` image, in `CV_8UC1` format.
 *   - _Output parameters_: Image `width` and `height` and the `motion-percentage` percentage of moving pixels.
 *
 * - <a name="load"></a> __[load]__ `{ do: load file: $image_file_name }` or `$image_file_name`.
 *   - Loads a BGR `CV_32UC3` image from, e.g. a `.png` or other standard file format.
 *   - _Input parameter_:
 *     - The `file` name, it can simply be given without JSON construct.
 *     - If the `alpha: true` option is present, an `alpha` transparency `CV_8UC1` channel is added, in any case.
 *     - `clear`: if not false image channels and parameters are beforehand cleared.
 *   - _Output channel_: The raw `input` image, in `CV_8UC3` format.
 *     - If transparency is present, a separate `alpha` transparency `CV_32SC1` channel is loaded, unless setting `alpha: false`.
 *   - _Output parameters_: Image `width` and `height`.
 *
 * - <a name="save"></a> __[save]__ `{ do: save path: $image_files_directory }`.
 *   - Saves all image channels and parameters in the target directory `path`.
 *   - _Input parameter_:
 *     - The `path`, files directory where to store the images.
 *   - _Output_: A `path` directory with
 *     - All cv::Mat images stored under their name.
 *       - Pixel images (i.e., 3 channels `CV_8U` image) are stored as `.png` image.
 *       - The `alpha` channel is stored in a distinct `CV_8C1` `.png` image.
 *       - Index images (i.e., 1 channel `CV_32S` image) are stored as floating point `.exr` images.
 *       - Floating point numeric images (i.e., `CV_32F` images) are stored as either 1 or 3 channel·s `.exr` images.
 *     - The `path/parameters.json` file contains all parameters
 *       - Including `channels` listing each `cv::Mat` data structure channel parameters.
 *
 * - <a name="cache"></a> __[cache]__ `{ do: cache name: ... what: (get|put|del[ete])}`.
 *   - Stores this image in a cache in order to use it as a source for other image processing.
 *   - _Input parameter_:
 *     - The unique `name` of the image image in the cache.
 *     - Defines `what` is to be done:
 *       - `put`: to put an image into the cache (default if the image name is undefined).
 *         - If an image is previously defined, it is erased by this operation.
 *       - `get`: to copy an image from the cache into this image (default if the image name is defined).
 *         - `clear`: if not false image channels and parameters are beforehand cleared.
 *       - `del` or `delete`: to delete an image which is in the cache.
 *   - _Output channels_: When `get`, all channels defined in the `cache` image.
 *   - _Output parameters_: When `get`, the `cache` image parameters.
 *
 * - <a name="copy"></a> __[copy]__ `{ do: copy }`.
 *   - Copies all channels of the `source` image into this image.
 *     - The `source` image defined either:
 *       - Using the `set(JSON parameters, const Image& source)` 2nd argument.
 *       - Or defining a `{ do: copy source: name}` parameter, getting the image from the cache by its name.
 *     - This is the default operation if no parameter is given.
 *   - _Input parameter_:
 *     - `clear`: if not false image channels and parameters are beforehand cleared.
 *   - _Input channels_: The source channels.
 *   - _Output channels_: All channels defined in the `source` image.
 *   - _Output parameters_: The `source` image parameters.
 *
 *  - <a name="crop"></a> __[crop]__ `{ do: crop, top: ..., right: ..., bottom: ..., left: ...}`
 *    - Crops a rectangular part of the `source` image into this target image.
 *    - _Input channel_: The source raw `input` image, in `CV_8UC3` format.
 *      - The `source` image defined either by the `set()` 2nd argument or using a `source` parameter, as for `{do: copy ...`.
 *    - _Input parameters_:
 *      - `clear`: if not false image channels and parameters are beforehand cleared.
 *      - `top`: Crop rectangle top vertical position, default is 0.
 *      - `right`: Crop rectangle right horizontal position, default is image.width-1.
 *      - `bottom`: Crop rectangle bottom vertical position, default is image.height-1.
 *      - `left`: Crop rectangle left horizontal position, default is 0.
 *    - _Output channel_: The raw `input` image, in `CV_8UC3` format.
 *
 *  - <a name="rectify"></a> __[rectify]__ `{ do: rectify, top_x: ..., top_y: ..., ...}`
 *    - Crops a quadrilateral of the `source` image into this target image, applying a perspective transform.
 *    - _Input channel_: The source raw `input` image, in `CV_8UC3` format.
 *      - The `source` image defined either by the `set()` 2nd argument or using a `source` parameter, as for `{do: copy ...`.
 *    - _Input parameters_:
 *      - `clear`: if not false image channels and parameters are beforehand cleared.
 *      - `top_x`: The crop top horizontal position, in pixel, default is 0.
 *      - `top_y`: The crop top vertical position, in pixel, default is 0.
 *      - `right_x`: The crop right horizontal position, in pixel, default is image.width-1.
 *      - `right_y`: The crop right vertical position, in pixel, default is 0.
 *      - `bottom_x`: The crop top horizontal position, in pixel, default is image.width-1.
 *      - `bottom_y`: The crop top vertical position, in pixel, default is image.height-1.
 *      - `left_x`: The crop top horizontal position, in pixel, default is 0.
 *      - `left_y`: The crop top vertical position, in pixel, default is image.height-1.
 *      - `width`: The final image width, default is the image width.
 *      - `height`: The final image height, default, default is the image height, or the width if specified.
 *    - _Output channel_: The raw `input` image, in `CV_8UC3` format.
 *
 * - <a name="intensity"></a> __[intensity]__
 *   - Calculates image intensity basic statistics, all blue, green and red values together
 *   - _Output parameters_: basic intensity statistical values:
 *     - `median` (image BGR intensity median intensity)
 *     - `lower-quartile`
 *     - `upper-quartile`
 *     - `minimum`
 *     - `maximum`
 *     - `mode`
 *     - `mode-count` (number of pixels with the mode value)
 *     - `mean`
 *     - `stdev` (standard-deviation)
 *     - `entropy` (intensity histogram entropy, between 0 and 8 bits for the 256 values)
 *     - `bin-count` (optimal histogram bin count according to [Scott rule](https://en.wikipedia.org/wiki/Histogram#Scott's_normal_reference_rule).
 *
 * - <a name="histogram"></a> __[histogram]__
 *   - Calculates image channel histograms:
 *   - _Input parameters_:
 *     - `what`: a sequence of unordered letter (default is all).
 *       - `B`: for the blue channel.
 *       - `G`: for the green channel.
 *       - `R`: for the red channel.
 *       - `H`: for the hue channel.
 *       - `T`: for the tint channel histogram, i.e. the hue channel weighted by the saturation.
 *       - `S`: for the saturation channel.
 *       - `V`: for the value channel.
 *       - `C`: for the contrast, i.e., the edge magnitude channel.
 *       - `O`: for the orientation, i.e., the edge orientation channel.
 *     - `window`: 1D smoothing window to avoid "sparse" histogram, default is none.
 *     - `proba`: If true, normalizes in order the sum of the values to be equal to 1.
 *     - `median`: If true, calculates the histogram median value in `target.get().["histogram"][what]["median"]` where `what` is the chosen channel.
 *       - The bin of value 0 is not taken into account, to avoid low value masking.
 *       - If a circular value is considered (i.e., an angle), this value is biased.
 *     - `mode`: If true, calculates the histogram mode value in `target.get().["histogram"][what]["mode"]` where `what` is the chosen channel.
 *       - The bin of value 0 is not taken into account, to avoid low value masking.
 *     - `smooth`: 1D histogram smoothing window to smooth histograms, default is 0 (no smoothing).
 *       - The bin of value 0 is not smoothed, to avoid low value masking.
 *       - For circular value, circular recursive smoothing is applied.
 *     - `draw`: either `lin` or `log` to draw the histogram with a linear or logarithmic scale (it applies only to the plot, the histogram remains unchanged).
 *       - `blue`: The blue channel.
 *       - `green`: The green channel.
 *       - `red`: The red channel.
 *       - `magenta`: The [hue](https://en.wikipedia.org/wiki/Hue) channel.
 *       - `pink`: The tint channel.
 *       - `yellow`: The saturation channel.
 *       - `white`: The value channel.
 *       - `gray`: The edge amplitude channel.
 *       - `cyan`: The edge orientation channel.
 *   - _Output channels_:
 *     - The `histogram_$letter` where `$letter` stands for the channel letter `B`, `R`, `G`, `H`, `S`, `V`, `C`, `O` with the histogram as a, in `CV_32FC1` format.
 *     - The `histogram_$letter-view` the histogram plots as an image, in `CV_8UC3` format, if `draw` is defined.
 *
 * - <a name="similarity"></a> __[similarity]__ `{do: similarity similarity: correlation ...`
 *   - Calculates the similarity with respect to the source image
 *   - _Input channels_:
 *     - This image channels, depending on the metric.
 *     - The `source` image defined either by the `set()` 2nd argument or using a `source` parameter, as for `{do: copy ...`.
 *     - The `alpha` channel if present in the source image is used as a weighting.
 *     - Input image pixel value at `255` are not taken into account (they are either saturated values or border values).
 *   - _Input parameters_:
 *     - `similarity`: the chosen similarity estimation
 *       - `correlation`: Uses the [normalized correlation](./Image.pdf#page=7) between the two images intensities. Returns a value in `[0, 4]`, `0` if entirely correlated, `2` if uncorrelated, `4` if entirely anti-correlated.
 *       - `hue`: Uses the hue mode value (i.e., most frequent value) difference, result between 0 and 180 degree.
 *       - `binary`: Uses the minimal hamming distance between then binary image and translated version of the reference image, result between `0` and `1`.
 *       - Other metric can be implemented via the [Metric](https://gitlab.inria.fr/line/aide-group/aidecv/-/blob/master/src/Metric.hpp) hook.
 *   - _Output parameters_:
 *     - `value`: The similarity value, a non-negative value, equal to `0` if similarity is maximal.
 *
 *  - <a name="balance"></a> __[balance]__ `{ do: balance, saturation: ... }`
 *    - Balances the image intensity with respect to white.
 *    - _Input parameters_:
 *      - `balance`: Method, either `simple` (default) or `grayworld` (or `learned`), as [defined in opencv](https://docs.opencv.org/3.4/d9/dcd/white__balance_8hpp.html).
 *      - `saturation`:
 *        - `simple` case: Percent of top/bottom values to ignore, default is `2`.
 *        - `grayworld` or `learned` case: Maximum saturation for a pixel to be included in the gray-world assumption, default is 0.9.
 *      - `equalize`: If true performs an BGR histogram equalization after balance.
 *    - _Input/Output channel_: The `input` image with white balance performed.
 *
 *  - <a name="median"></a> __[median]__ `{do: smooth window: ...}`
 *    - Computes a smoothed version of this image using a median filter.
 *    - _Input parameters_:
 *      - `window` The smoothing filter window size in pixel, typically `3` (default), `5` or `7`.
 *    - _Input/Output channel_: The `input` image with median filtered performed.
 *
 *  - <a name="fovea"></a> __[fovea]__ `{ do: fovea, }`
 *   - Defines a foveal mask of transparency to limit image border influence.
 *    - Implements a fixed quadratic separable profile.
 *    - _Input/Output channel_: The `alpha` transparency `CV_8UC1` channel, if existent, created otherwise.
 *
 *  - <a name="resize"></a> __[resize]__ `{ do: resize, width: ..., height: ... }`
 *    - Resizes all channel of this image to the given sizes, or to the source image sizes (_without_ copying the source image).
 *    - _Input parameters_:
 *      - `width`: Image width, default is the source image width if any, else unchanged.
 *      - `height`: Image height, default is the source image height if any, else unchanged
 *    - _Output parameters_: The new `width` and `height`.
 *    - _Input/Output channel_: All image channels at the new size.
 *
 *  - <a name="bgr"></a> __[bgr]__ `{ do: bgr }` or `bgr`.
 *    - Computes the image as a `CV_32FC3` 3 channels float.
 *    - _Input channel_: The `input` raw image in `CV_8UC3` format.
 *    - _Output channel_: A raw `bgr` blue-green-red image, in `CV_32FC3` format, with values in `[0, 1]`.
 *
 *  - <a name="hsv"></a> __[hsv]__ `{ do: hsv }` or `hsv`.
 *    - Computes the image [BGR->HSV color conversion](https://docs.opencv.org/3.4/de/d25/imgproc_color_conversions.html), as a `CV_32FC3` 3 channels float.
 *      - The [hue](https://en.wikipedia.org/wiki/Hue) angular value stands between 0deg and 360deg
 *      - The saturation relative value stands between 0 and 1
 *      - The (maximal intensity) value stands between 0 and 1
 *    - _Input channel_: The `bgr` blue-green-red image, in `CV_32FC3` format.
 *    - _Output channel_: A raw `hsv` hue-saturation-value image, in `CV_32FC3` format.
 *
 *  - <a name="smooth"></a> __[smooth]__ `{do: smooth window: ...}`
 *    - Computes a smoothed version of this image using a [1st order exponential recursive filter](./Image.pdf#page=2), as a `CV_32FC3` 3 BGR channels float.
 *    - _Input channel_: The `bgr` blue-green-red image, in `CV_32FC3` format.
 *    - _Input parameters_:
 *      - `window`: The smoothing filter window size in pixel, with 90% of the signal to be in a the pixel window.
 *    - _Output channel_: A `smooth` blue-green-red image, in `CV_32FC3` format.
 *
 *  - <a name="edge"></a> __[edge]__ `{do: edge window: ...}`
 *    - Computes a the local edge characteristics.
 *      - Magnitude: the edge magnitude, from 0 to 255, in pixel value.
 *      - Orientation: the edge orientation, from 0 to 360 degree.
 *    - It is based on the local intensity derivative [SVD](https://en.wikipedia.org/wiki/Singular-value_decomposition) we obtain the [color edge eigen elements](./Image.pdf#page=3), using a smoothed image.
 *    - _Input channel_: The `smooth` blue-green-red image, in `CV_32FC3` format.
 *    - _Input parameters_:
 *      - `window` The smoothing filter window size in pixel, with 90% of the signal to be in a the pixel window.
 *    - _Output channel_: A `edge` intensity-orientation image, in `CV_32FC2` format, the 3rd channel not being used.
 *
 *  - <a name="subtract"></a> __[subtract]__ `{ do: subtract, threshold: ..., slope: ...}`
 *    - Background thresholds this image, considering the `source` image as background.
 *    - _Input channel_:
 *      - The source raw `input` image, in `CV_8UC3` format.
 *      - The `source` image defined either by the `set()` 2nd argument or using a `source` parameter, as for `{do: copy ...`.
 *    - _Input parameters_:
 *      - `threshold`: The intensity threshold above which it is not in the background, default is `50`.
 *      - `mode`: The threshold mechanism:
 *        - `step`: To set to zero values below a given BGR threshold (default).
 *        - `weak`: To progressively reduce the intensity below a given BGR threshold.
 *    - _Input/Output channels_:
 *        - The `input` image with the multiplicative subtraction done, in `CV_8UC3` format.
 *        - The `subtract` image with a copy of the input image before subtraction, in `CV_8UC3` format.
 *
 *  - <a name="blob"></a> __[blob]__ `{do: blob blue: ... green: ... red: ... ... }`
 *    - Detects color blobs in the image.
 *    - The HSV [color conversion](https://docs.opencv.org/3.4/de/d25/imgproc_color_conversions.html) is considered:
 *    - The pixel hue, intensity and color saturation are taken into account.
 *    - The method is derived from [here](https://docs.opencv.org/3.4/da/d97/tutorial_threshold_inRange.html) and [there](https://docs.opencv.org/trunk/df/d9d/tutorial_py_colorspaces.html).
 *    - _Input channel_: The raw `hsv` hue-saturation-value image, in `CV_32FC3` format.
 *    - _Input parameters_:
 *      - `name`: The channel name for subsequent invocation to this blob.
 *      - `blue`: The blob median blue color, between 0 and 255, default is 128.
 *      - `green`: The blob median green color, between 0 and 255, default is 128.
 *      - `red`: The blob median red color, between 0 and 255, default is 128.
 *      - `hue_threshold`: The color hue threshold, in degree, in [0deg, 180deg], values in [hue_blob - hue_threshold, hue_blob + hue_threshold] are considered, default is 10deg.
 *      - `saturation_threshold`: The color saturation minimal value, in percentage, values between [saturation_threshold, 100%] are considered, default is 0%.
 *      - `value_threshold`: The intensity minimal value, values between [intensity_threshold, 255] are considered, default is 0.
 *    - _Output channel_: A `blob` integer mask as a `CV_32SC1` image with:
 *      - `1`: for blob pixel value,
 *      - `0`: for non-blob pixel value.
 *
 *  - <a name="segment"></a> __[segment]__ `{do: segment input:... ... }`
 *    - _Input channel_: An integer mask as a `CV_32SC1` image.
 *    - _Input parameters_:
 *      - `input`: The input mask channel, segments image blobs according to the `blog_threshold` or `background_threshold` input channel `CV_32S1` integer channel information.
 *        - Adjacent pixel of the same region have the same non-negative value in the mask.
 *        - Negative values corresponds to unlabeled pixels to be included in the closest indexed region.
 *      - `size_threshold`: The minimal pixel count of a blob, default is 100.
 *      - `draw`: If defined draw the blobs on a `output` BGR image, specifying what is to be drawn, any bitwise combination of:
 *       - `0x01`: Draws the "name" (or "index" if "name" is undefined) as a text around the blob center ("x", "y").
 *       - `0x02`: Draws a '+' cross on the blob center ("x", "y") with a length related to the "size".
 *       - `0x04`: Draws a rectangle from the blob "top", "right", "bottom", "left", border.
 *       - `0x08`: Draws an ellipse of center ("x", "y"), angle "a", and axes length ("L", "l")
 *       - `0x10`: Draws the pixels corresponding to the blob, using the blob average intensity.
 *       - `0x20`: Draws the pixels corresponding to the blob, using a gray color.
 *
 *    - _Output parameters_: In `segment/blobs`, a list of blobs, in size decreasing order, each blob defined by:
 *      - `index`: The blob index, form 0 number of pixels.
 *      - `size`: The blob number of pixels.
 *      - `x`: The blob horizontal median position, in pixel.
 *      - `y`: The blob vertical median position, in pixel.
 *      - `a`: The blob main orientation, in degree.
 *      - `L`: The blob 2nd larger axis length.
 *      - `l`: The blob 2nd smaller axis length.
 *      - `e`: The blob thinness, from 0% if circular to 100% if rectilinear.
 *      - `top`: The blob top minimal vertical position, in pixel.
 *      - `right`: The blob right maximal horizontal position, in pixel.
 *      - `bottom`: The blob bottom maximal vertical position, in pixel.
 *      - `left`: The blob left minimal horizontal position, in pixel.
 *      - `blue`: The average blob intensity blue value.
 *      - `green`: The average blob intensity green value.
 *      - `red`: The average blob intensity red value.
 *    - _Output channels_:
 *      - `segment`: Generates a `CV_32SC1` integer index map with a non-negative index to label each detected region.
 *      - `output`: Generates a `CV_8UC2` image with the blobs drawn if the input parameter is defined.
 *    - Algorithm details:
 *      - Segments an image according to [a standard colorization algorithm](./Image.pdf#page=5).
 *      - Adjacent pixels in 8-connectivity (horizontal or vertical, and oblique) belongs to the same region.
 *
 * - <a name="match"></a> __[match]__ `{do: match transform: [{ ../..}], source: ..}`
 *   - Matches the source image·s with this image optimizing the given transform with respect to normalized correlation.
 *   - _Input channels_:
 *     - This image "input" channel.
 *     - The `source` image defined either by the `set()` 2nd argument or using a `source` parameter, as for `{do: copy ...`.
 *   - _Input parameters_:
 *     - `parameters`: The [parameters](./Image.pdf#page=8) sequence, a [numeric](https://line.gitlabpages.inria.fr/aide-group/stepsolver/Numeric.html) structure of the form:
 *  ```
 *  [
 *    {
 *      translation_i: { precision: 1    min: -width  max: width  step: 20 unit: pixel  }
 *      translation_j: { precision: 1    min: -height max: height step: 20 unit: pixel  }
 *      rotation:      { precision: 0.01 min: -10     max: 10     unit: radian }
 *      zoom:          { precision: 0.01 min: 0.1     max: 10 zero: 1 }
 *      warp:          { precision: 0.01 min: -10     max: 10 }
 *      twist:         { precision: 0.01 min: -10     max: 10 }
 *    }
 *    // Other multi-scale mapping process
 *  ]
 *  ```
 *   - _Output parameters_:
 *     - `similarity`: the best obtained similarity.
 *     - `transform`: the best [transform](./Image.pdf#page=8) parameters: `translation`: [t_x, t_y] in pixel, `rotation`: in linearized radian, `zoom`: in image ratio (`1` if no zoom), `warp` and `twist`.
 *     - `results`: The input parameters adjusted after matching.
 *
 *  - <a name="draw"></a> __[draw]__ `{ do: draw, what: ... }`
 *    - Draws 2D elements on a copy of the input image.
 *    - _Input channel_: The source raw `input` image, in `CV_8UC3` format.
 *    - _Input parameters_:
 *      - `what`: A list of 2D elements of the form:
 *        - `{do: text x:... y:... text:... color:...}` for a text `text` at a given position `(x,y)`.
 *        - `{do: cross x:... y:... l:... color:...}` for a `+` cross at `(x,y)` and of length `l`.
 *        - `{do: line x1:... y1:... x2:... y2:... color:...}` for a line segment between `(x1,y1)` and `(x2,y2)`.
 *        - `{do: rectangle left:... top:... right:... bottom:... color:...}` for a rectangle.
 *        - `{do: quadrilateral top_x:... top_y:... right_x:... right_y:... bottom_x:... bottom_y:... left_x:... left_y:... color:...}` for a rectangle.
 *        - `{do: circle x:... y:... r:... color:...}` for a circle of center `(x,y)` and radius `r`.
 *        - `{do: ellipse x:... y:... a:... l:... L:... color:...}` for an ellipse of center `(x,y)` angle `a` in degree and axes length `L`, `l`.
 *      - For compatibility reason `crop` is a synonym of `rectangle` and `rectify` is a synonym of `quadrilateral`.
 *      - The color is given as a [w0rgbycm] char for ([w]hite, [0]black, [r]ed, [g]reen, [b]lue, [y]ellow, [c]yan, [m]agenta) color.
 *
 *    - _Output channel_: The `draw` image with the `input` image and the drawings.
 *
 *  - <a name="show"></a> __[show]__ `{ do: show, name: ... channel: ... }`
 *    - Displays channel·s in popup window·s.
 *    - _Input channel·s_: The channel to show.
 *    - _Input parameters_:
 *      - `name`: Optional image name for the window title.
 *      - `channel`: A channel name, default is `channel: input`.
 *      - `channels`: A list of channel names, e.g. `channels: [input, edge]`, that must have been precalculated.
 *    - The `Image::waitUntilWindowsClosed()` is usually called at the end of the `int main(int argc, char *argv[])` function, allowing to maintain the program until all windows are closed.
 *
 * ## Caution about bug and caveat <a name="bugs"></a>
 *
 * - There is a bug using the opencv while forking processes, the program may block.
 */
