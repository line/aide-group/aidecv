#ifndef __aidecv_Image__
#define __aidecv_Image__

#include "std.hpp"
#include "Value.hpp"
#include <map>
#include <set>
#include <opencv2/core/core.hpp>

namespace aidecv {
  class SpotEventDetector;

  /** Implements image processing, at a symbolic level, in this context.
   * - An Image is defined by a:
   *   - A set of channels storing image computations.
   *   - A set of parameters extracted from image computations.
   * - The implementation is based on [opencv](https://docs.opencv.org/4.0.0).
   * - Available commands are defined as [image setters](parameters.html).
   *
   * @class Image
   * @param {string} [command=copy] The command input parameters.
   * @param {Image} [source] An optional source image, if needed.
   */
  class Image {
public:
    Image(JSON command, const Image& source);
    Image(String command, const Image& source);
    Image(const char *command, const Image& source);
    Image(const Image& source);
    Image(JSON command);
    Image(String command);
    Image(const char *command);
    Image(unsigned int command);
    Image();
    ~Image();

    /**
     * @function set
     * @memberof Image
     * @instance
     * @description Sets channels and parameters of this image.
     * - Available commands are defined as [image setters](commands.html);
     * - Other ``setter´´ mechanisms can be implemented via the [Setter](https://gitlab.inria.fr/line/aide-group/aidecv/-/blob/master/src/Setter.hpp) hook.
     * @param {string} command The command input command.
     * @param {Image} [source] An optional source image, if needed.
     * @return {Image} This image, allowing construct of the form `image.set("{...").set("{...")`.
     */
    Image& set(JSON command, const Image& source);
    Image& set(String command, const Image& source);
    Image& set(const char *command, const Image& source);
    Image& set(const Image& source);
    Image& set(JSON command);
    Image& set(String command);
    Image& set(const char *command);
    Image& set(unsigned int command);

    /**
     * @member {Map} channels
     * @memberof Image
     * @instance
     * @description Low-level access to the Image internal elements
     *  - The `std::map < std::string, cv::Mat > channels` map of opencv `cv::Mat` is a read/write data struture member.
     * - Each input/output channel is described in the corresponding setter description.
     */
    std::map < std::string, cv::Mat > channels;

    /**
     * @member {Value} parameters
     * @memberof Image
     * @instance
     * @description Read/write access to the image parameters.
     * - By contract each setter modifies the `parameters["setter-name"]` data structure.
     * - The `wjson::Value parameters` data structure contains all information on image processing and obtained values.
     * - Each input/output parameters are described in the corresponding setter description.
     */
    wjson::Value parameters;

    // Clears all image channels and parameters
    void clear();
    // Gets the image input alpha channel, creating it if needed
    cv::Mat& getAlpha();
    // Waits until all windows opened by the show command are closed.
    static void waitUntilWindowsClosed();
    // Optional additional objects of the Image class
    SpotEventDetector *spotter = NULL;
  };
}
#endif
