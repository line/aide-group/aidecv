#include "Image.hpp"
#include "Setter.hpp"
#include "file.hpp"
#include "regex.hpp"
#include "opencv.hpp"

namespace aidecv {
  // Empty image used as default parameter
  Image emptyImage;
  // Image cache to put/get/del images by their name, see cache.hpp
  std::map < std::string, Image > images;
  // Manages reduced form of do actions
  wjson::Value string2json(String command)
  {
    return wjson::string2json(aidesys::regexMatch(command, "\\s*[0-9]+\\s*") ?
                              "{do: grab device: " + command + "}" :
                              command[0] != '{' && aidesys::exists(command) ?
                              "{do: load file: '" + aidesys::regexReplace(command, "'", "\\'") + "'}" :
                              aidesys::regexMatch(command, "\\s*[a-z_]+\\s*") ?
                              "{do: " + command + "}" :
                              command, true);
  }
  //
  // Implements the public interface
  //
  Image::Image(JSON command, const Image& source)
  {
    set(command, source);
  }
  Image::Image(String command, const Image& source)
  {
    set(aidecv::string2json(command), source);
  }
  Image::Image(const char *command, const Image& source)
  {
    set(aidecv::string2json((String) command), source);
  }
  Image::Image(const Image& source)
  {
    set("{do: copy}", source);
  }
  Image::Image(JSON command)
  {
    set(command, emptyImage);
  }
  Image::Image(String command)
  {
    set(aidecv::string2json(command), emptyImage);
  }
  Image::Image(const char *command)
  {
    set(aidecv::string2json((String) command), emptyImage);
  }
  Image::Image(unsigned int command)
  {
    set(command);
  }
  Image::Image()
  {}
  Image& Image::set(String command, const Image& source)
  {
    return set(aidecv::string2json(command), source);
  }
  Image& Image::set(const char *command, const Image& source)
  {
    return set(aidecv::string2json((String) command), source);
  }
  Image& Image::set(const Image& source)
  {
    return set("{do: copy}", source);
  }
  Image& Image::set(JSON command)
  {
    return set(command, emptyImage);
  }
  Image& Image::set(String command)
  {
    return set(aidecv::string2json(command), emptyImage);
  }
  Image& Image::set(const char *command)
  {
    return set(aidecv::string2json((String) command), emptyImage);
  }
  Image& Image::set(unsigned int command)
  {
    return set(aidecv::string2json(aidesys::echo("{do: grab device: %d}", command)), emptyImage);
  }
  //
  // Implements the set hook mechanism
  //
  Image& Image::set(JSON command, const Image& source)
  {
    // Retrieves what is to be done
    aidesys::alert(!command.isRecord(), "illegal-argument", "in aidecv:Image::set, spurious command: '" + command.asString() + "'");
    aidesys::alert(!command.isMember("do"), "illegal-argument", "in aidecv:Image::set, undefined `do` operation: '" + command.asString() + "'");
    std::string action = command.get("do", "");
    // Checks if already in the cache with the same command
    {
      // Defines which operation are cached
      std::set < std::string > caches = { "intensity", "histogram", "bgr", "hsv", "smooth", "edge", "blob" };
      auto it = caches.find(action);
      // - printf("=> set() {command: '%s' cachable: %d previous_command: '%s' cached : %d}\n", command.asString().c_str(), it != caches.end(), parameters.at("done").at(action).asString().c_str(), parameters.at("done").at(action) == command);
      if(it != caches.end() && parameters.at("done").at(action) == command) {
        return *this;
      }
    }
    // Applies a setter
    auto setters = Setter::getSetters();
    auto it_setters = setters.find(action);
    aidesys::alert(it_setters == setters.end(), "illegal-argument", "in aidecv:Image::set, unimplemented do = '" + action + "' operation in '" + command.asString() + "'");
    const Image *ptr_source = &source;
    // Manages the fact the source is obtained by name from the images cache
    if(command.isMember("source")) {
      std::string name = command.get("source", "");
      aidesys::alert(ptr_source != &emptyImage, "illegal-argument", "in aidecv:Image::set, source image is defined by both a cache name and a non empty source argument: '" + command.asString() + "'");
      auto it = images.find(name);
      aidesys::alert(it == images.end(), "illegal-argument", "in aidecv:Image::set, source image is undefined in the cache: '" + command.asString() + "'");
      ptr_source = &it->second;
    }
    // Applies the setter
    {
      (it_setters->second)->set(*this, command, *ptr_source);
      parameters["done"][action] = command;
      if(ptr_source != &emptyImage) {
        parameters["done"][action]["source"] = ptr_source->parameters;
      }
    }
    return *this;
  }
  void Image::clear()
  {
    channels.clear();
    parameters.clear();
  }
  cv::Mat& Image::getAlpha()
  {
    if(channels.find("alpha") == channels.end()) {
      const cv::Mat& img = channels.at("input");
      channels["alpha"] = cv::Mat(cv::Size(img.cols, img.rows), CV_8UC1, cv::Scalar(255));
    }
    return channels.at("alpha");
  }
  void Image::waitUntilWindowsClosed()
  {
    opencvwindow::waitUntilWindowsClosed();
  }
}

// All implemented mechanisms
#include "inc/load.hpp"
#include "inc/grab.hpp"
#include "inc/spot.hpp"
#include "inc/save.hpp"
#include "inc/copy.hpp"
#include "inc/cache.hpp"
#include "inc/resize.hpp"
#include "inc/crop.hpp"
#include "inc/rectify.hpp"
#include "inc/intensity.hpp"
#include "inc/histogram.hpp"
#include "inc/similarity.hpp"
#include "inc/balance.hpp"
#include "inc/median.hpp"
#include "inc/fovea.hpp"
#include "inc/bgr.hpp"
#include "inc/hsv.hpp"
#include "inc/smooth.hpp"
#include "inc/edge.hpp"
#include "inc/subtract.hpp"
#include "inc/blob.hpp"
#include "inc/segment.hpp"
#include "inc/match.hpp"
#include "inc/draw.hpp"
#include "inc/show.hpp"

namespace aidecv {
  Image::~Image()
  {
    delete spotter;
  }
}
