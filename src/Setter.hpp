#ifndef __aidecv_Setter__
#define __aidecv_Setter__

#include "Image.hpp"
#include "std.hpp"

/**
 * @class aidecv~Setter
 * @memberof aidecv
 * @description Template to implement a setter hook mechanism.
 * A typical implementation is of the form:
 *
 * ```
 * #include "Setter.hpp"
 * namespace aidecv {
 *   class SomethingSetter : public Setter {
 *   public:
 *     SomethingSetter() : Setter("something"){}
 *     void set(Image& target, JSON command, const Image& source_if_any) {
 *       // Implements the setter
 *     }
 *   } somethingSetter;
 * }
 *```
 * @param {string} name The 'do' operation name to register for this setter.
 */
namespace aidecv {
  class Setter {
protected:
    // Declares this setter for a given 'do' operation
    Setter(String what);
public:

    /**
     * @function set
     * @memberof aidecv~Setter
     * @instance
     * @description Implements the setter operation.
     * - Typically modifies the target given the command, optionnaly using the source.
     * @param {Mat} target The input/output image.
     * @param {JSON} command The command parameters.
     * @param {Mat} source The source image if any, otherwise an empty image.
     */
    virtual void set(Image& target, JSON command, const Image& source);

    /**
     * @function getSetters
     * @memberof aidecv~Setter
     * @static
     * @description Returns the setters table
     * @return {Map} A `std::map < std::string, Setter * >` with the register setters indexed by the 'do' operation name.
     */
    static std::map < std::string, Setter * >& getSetters();
  };
}

#endif
