#ifndef __aidecv_Similarity__
#define __aidecv_Similarity__

#include "Image.hpp"
#include "std.hpp"

/**
 * @class aidecv~Similarity
 * @memberof aidecv
 * @description Template to implement a similarity hook mechanism.
 * A typical implementation is of the form:
 * ```
 * #include "Similarity.hpp"
 * namespace aidecv {
 *   class SomethingSimilarity : public Similarity {
 *   public:
 *     SomethingSimilarity() : Similarity("something") {}
 *     double get(Image& target, const Image& source) {
 *       const cv::Mat& img = target.channels.at("input"), src = source.channels.at("input"), alpha = source.channels.at("alpha");
 *       // Implements the similarity
 *     }
 *   } somethingSimilarity;
 * }
 *```
 * @param {string} name The 'similarity' operation name to register for this similarity.
 */
namespace aidecv {
  class Similarity {
protected:
    // Declares this similarity for a given 'similarity' operation
    Similarity(String similarity);
public:

    /**
     * @function set
     * @memberof aidecv~Similarity
     * @instance
     * @description Implements the similarity operation.
     * @param {Mat} target The input/output image.
     * @param {Mat} source The source image to compare with.
     * @return {double} The similarity value.
     */
    virtual double get(Image& target, const Image& source);

    /**
     * @function getSimilarities
     * @memberof aidecv~Similarity
     * @static
     * @description Returns the similarity table
     * @return {Map} A `std::map < std::string, Similarity * >` with the register similarities indexed by the 'similarity' operation name.
     */
    static std::map < std::string, Similarity * >& getSimilarities();
  };
}
#endif
