# aidecv

Image processing encapsulation via web interface

@aideAPI

This package provides an abstract interface with [opencv](https://docs.opencv.org/4.0.0) image processing functions and additional algorithms at a symbolic level to be used, e.g., via a web interface.

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidecv'>https://gitlab.inria.fr/line/aide-group/aidecv</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidecv'>https://line.gitlabpages.inria.fr/aide-group/aidecv</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidecv/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/aidecv/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/aidecv'>softwareherirage.org</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/aidecv.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>stepsolver: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/stepsolver'>A step by step variational solver mechanism</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Author

- Thierry Vieville <thierry.vieville@inria.fr>

