%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}


\vspace{4cm} \centerline{\Huge Derivations used in {\tt Image.hpp} calculations}

\newpage \subsubsection*{First order smoothing recursive filtering}
     
Given a raw image, the smoothed version of this image using a 1st order exponential recursive filter, 
writes for each channel:
\eqline{output[i, j] = \sum_{uv} c \, a^{|u-i||v-j|} \, input[u, v], \; c \deq \left(\frac{1+a}{1-a}\right)^2}

The parameter is related to the smoothing filter window size in pixel, with 90\% of the signal to be in a the pixel window:
\eqline{a = (1 - p)^\frac{1}{window}}
with $p = 0.9$.

The implementation is a simple four directional 1st order recurrent filter, writing for a $\{0, H\{ \times \{0, V\{$ discrete image:
\eqline{\begin{array}{rcll}
output[i, j] &\leftarrow& a \, output[i-1, j] + (1 - a) \, input[i, j] & i = 1 \cdots H-1 \\
output[i, j] &\leftarrow& a \, output[i+1, j] + (1 - a) \, output[i, j] & i = H-2 \cdots 0 \\
output[i, j] &\leftarrow& a \, output[i, j-1] + (1 - a) \, output[i, j] & j = 1 \cdots V-1 \\
output[i, j] &\leftarrow& a \, output[i, j+1] + (1 - a) \, output[i, j] & j = V-2 \cdots 0 \\
\end{array}}

In particular the computation time does not depend on the smoothing window.

\newpage \subsubsection*{Color edge eigen elements}

Given a blue, red, green $(b,g,r)$ three-channel color image the local intensity derivative $3 \times 2$ matrix SVD writes:
\eqline{{\bf J}({\bf p}) \deq \partial_{\bf p} {\bf i}({\bf p}) = 
\underbrace{\left(\begin{array}{cc} k_b & l_b \\ k_g & l_g \\ k_r & l_r \\ \end{array} \right)}_{\bf U} \, \left(\begin{array}{cc} G & 0 \\ 0 & g \end{array}\right) \, \left(\begin{array}{cc} \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta) \end{array}\right)  , {\bf p} \deq (i, j)^T, {\bf i} \deq (b, g, r)^T}
with ${\bf U}^T \, {\bf U} = {\bf I}$ and we easily obtain, writing 
$J_{xx} \deq \sum_{k\in\{b,g,r\}} J_{kx}^2$,
$J_{xx} \deq \sum_{k\in\{b,g,r\}} J_{kx} \, J_{ky}$,
$J_{yy} \deq \sum_{k\in\{b,g,r\}} J_{ky}^2$,
\eqline{\left\{\begin{array}{rcll}
 \sqrt{J_{xx} + J_{yy}} &=& \sqrt{G^2 + g^2} & \mbox{edge magnitude} \\
 \frac{1}{2} \arctan\left(\frac{2 \, J_{xy}}{J{yy} - J_{xx}} \right) &=& \theta & \mbox{edge orientation}
\end{array} \right.}
as verified by the following piece of code:

{\scriptsize \begin{verbatim}
with(LinearAlgebra):
J := Matrix([[k_b, l_b], [k_g, l_g], [k_r, l_r]]) . Matrix([[G, 0], [0, g]]) . Matrix([[cos(theta), -sin(theta)], [sin(theta), cos(theta)]]):
eq := simplify({
 Jxx = J[1][1]^2 + J[2][1]^2 + J[3][1]^2,
 Jxy = J[1][1] * J[1][2] + J[2][1] * J[2][2] + J[3][1] * J[3][2],
 Jyy = J[1][2]^2 + J[2][2]^2 + J[3][2]^2
}, {
 k_b^2 + k_g^2 + k_r^2 = 1,
 k_b * l_b + k_g * l_g + k_r * l_r = 0,
 l_b^2 + l_g^2 + l_r^2 = 1
}):
zero := factor(combine(simplify(subs(eq, {
Jxx + Jyy                     - (G^2 + g^2),
2 * Jxy / (Jyy - Jxx)         - tan(2 * theta),
(Jyy - Jxx)^2 + (2 * Jxy)^2   - (G^2 - g^2)^2,
Jxx * Jyy - Jxy^2             - G^2 * g^2
}))));
\end{verbatim}}

\newpage It generalizes the mono-channel monochrome edge magnitude $G \deq \sqrt{I_x^2 + I_y^2}$ and orientation $\theta = -\arctan\left(I_y/I_x\right)$, or a three-channel color image with proportional gradients, i.e., with $k_b^2 + k_g^2 + k_r^2 = 1$, while $k_g = k_r = 0$ in the mono-channel case: 
\eqline{{\bf J}({\bf p}) = \partial_{\bf p} {\bf i}({\bf p}) \deq \left(\begin{array}{c} k_b \\ k_g  \\ k_r \\ \end{array} \right) \, (I_x, I_y) = \underbrace{\left(\begin{array}{c} k_b \\ k_g  \\ k_r \\ \end{array} \right) \, (1, 0)}_{\bf U} \, \left(\begin{array}{cc} G & 0 \\ 0 & 0 \\  \end{array} \right) \, \left(\begin{array}{cc} \frac{I_x}{G} & \frac{I_y}{G} \\ -\frac{I_y}{G} & \frac{I_x}{G} \\  \end{array} \right),}
i.e., it corresponds to the case where $g = 0$. 

As a consequence, $g/G$ provides an indication of how much the three-channels are linearly coherent. 

Then the tricky point is to manage the angle $\theta$ in the four quadrants, since the angle $\theta$ is estimated in $[-\Pi/2, \Pi/2]$, and in fact only up to a $\Pi$ constant (adding $\Pi$ in the equation does not modified then). we thus must estimate the direction of the gradient to obtain this $\Pi$ constant. Here we estimate this direction averaging the gradient of the three channels, i.e., use the following experimental rule, writing 
$J_{x} \deq \sum_{k\in\{b,g,r\}} J_{kx}$ and
$J_{y} \deq \sum_{k\in\{b,g,r\}} J_{ky}$, in order to obtain $\theta \in [0, 2 \Pi[$:
\eqline{\begin{array}{rcl}
 \theta &\leftarrow& \mbox{if } \theta < 0 \mbox{ then } \Pi + \theta \mbox{ else } \theta \\
 \theta &\leftarrow& \mbox{if } J_y > 0 \mbox{ or } (J_x > 0 \mbox{ and } J_y = 0) \mbox{ then } \Pi + \theta \mbox{ else } \theta, \\
\end{array}}
as visible for this test image:
\\ \centerline{\includegraphics[width=0.3\textwidth]{./test_edge.png}}
remembering that the vertical axis is up side down in images, and noticing that the image has been smoothed before the edge calculation, in order to reduce the discretization local errors. The left image is a circular blob, and the right image displays the edge magnitude in blue, and the edge orientation in red, from 0 to 360 degree.

\newpage \subsubsection*{Image blob segmentation, using a standard colorization algorithm} 

Here are the algorithm and derivations used in \href{./Image.html#blobs_segment}{\tt Image::channel(\{do: blob\_segments}.

We consider an image with a background and ``blobs'', i.e., disconnected regions.

We consider as input binary image (a ``calque'') {\tt I[p] $\in \{-1, M\{$, p = (i, j) $\in \{0, width\{ \times \{0, height\{$}, where $M \deq width \, height$ is the image size, while {\tt I[p] < 0} outside a blob.

On output {\tt I[p] $\in \{-1, M\{$} corresponds to the index of the obtained blob.

Here we consider the following indexing: {\tt I[p] = q, q = i + width j}, stating that the pixel at {\tt p} is chained to the pixel at {\tt q}, unless {\tt I[p] < 0}, i.e. a pixel outside a blob.

We consider the following algorithm to collect the different connected blobs:

\begin{verbatim}
- Initialization: Each pixel is considered as a singleton, i.e., a blob of size 1, setting:
   I[p] = p
- Repeat for each pixel:
  - Chained index reduction: Each pixel index is set to the smallest value, i.e.:
   while(I[I[p]]< I[p]) I[p] = I[I[p]]
  - Merge connected pixels, horizontaly and verticaly (i.e., 4-connectivity), i.e.:
   if (0 <= I[i, j] && 0 <= I[i-1, j]) I[i, j] = I[i-1, j] = min(I[i, j], I[i-1, j])
   if (0 <= I[i, j] && 0 <= I[i, j-1]) I[i, j] = I[i, j-1] = min(I[i, j], I[i, j-1])
  Untill no more index change.
\end{verbatim}

At the convergence, each pixel index corresponds to the blob smallest pixel index.

\newpage Then each blob are scanned in order to calculate basic parameters: size, centroids ${\bf c}$, enclosing rectangle, second order momenta geometry, i.e. using the very standard derivation:
\eqline{\begin{array}{rcl}
m_{pq} &\deq& \sum_{ij} I[i, j] \, i^p \, j^q, \\
{\bf c} &\deq& \left(\frac{m_{10}}{m_{00}}, \frac{m_{01}}{m_{00}}\right)^T, \\
\mu_{pq} &\deq& \sum_{ij} I[i, j] \, (i - c[0])^p \, (j - c[1])^q, \\
{\bf C} &\deq& \left(\begin{array}{cc} \mu_{20} & \mu_{11} \\ \mu_{11} & \mu_{02} \\ \end{array}\right) = 
\left(\begin{array}{cc} \cos(\theta) & \sin(\theta) \\ -\sin(\theta) & \cos(\theta) \\ \end{array}\right) \,
\left(\begin{array}{cc} L & 0 \\ 0 & l \\ \end{array}\right)
\left(\begin{array}{cc} \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta) \\ \end{array}\right).
\end{array}}
yielding:
\eqline{\left\{\begin{array}{rcl}
L &=& \frac{\mu_s + \mu_q}{2}, \\
l &=& \frac{\mu_s - \mu_q}{2}, \\
\tan(2\, \theta) &=& \frac{-2 \, \mu_{11}}{\mu_d}, \\ 
e &=& \frac{\mu_q}{\mu_s}, \\
\end{array}\right. \mbox{with} \left\{\begin{array}{rcl} 
 \mu_s &\deq& \mu_{20} + \mu_{02}, \\
 \mu_d &\deq& \mu_{20} - \mu_{02},  \\
 \mu_q &\deq& \sqrt{\mu_d^2 + \mu_{11}^2}. \\
\end{array}\right.}

Here $L \geq l$ and the thinness is $0$ for a circular form and $1$ for a line segment, as verified by this piece of {\tt maple} algebra:

{\scriptsize \begin{verbatim}
with(LinearAlgebra):
assume(mu_20 > 0, mu_02 > 0, mu_11 :: real):
Mu := Matrix([[mu_20, mu_11], [mu_11, mu_02]]):
mu_R := Matrix([[cos(theta), -sin(theta)], [sin(theta), cos(theta)]]):
mu_D := Matrix([[L, 0], [0, l]]):

mu_s := mu_20 + mu_02:
mu_d := mu_20 - mu_02:
mu_q := sqrt(mu_d^2 + mu_11^2):
sl := {
  L = (mu_s + mu_q)/2,
  l = (mu_s - mu_q)/2,
  sin(theta_2) = -2 * mu_11 / mu_q,
  cos(theta_2) = mu_d / mu_q
}:
zero := simplify(subs(sl, combine(subs(theta = theta_2 / 2, simplify(convert(Mu - Transpose(mu_R) . mu_D. mu_R, set))))));
\end{verbatim}}

\newpage \subsubsection*{Normalized correlation similarity}

We consider the standard \href{https://en.wikipedia.org/wiki/Digital_image_correlation_and_tracking#Overview}{image correlation ratio} between two images $I_0[i,j]$ and $I_1[i, j]$ of the same sizes $S = width \, height$, writing:
\eqline{\mu_0 \deq \frac{1}{S} \, \sum_{ij} I_0[i, j], \mu_1 \deq \frac{1}{S} \, \sum_{ij} I_0[i, j], \sigma_0 \deq \sqrt{\frac{1}{S-1} \, \sum_{ij} (I_0[i, j] - \mu_0)^2}, \sigma_1 \deq \sqrt{\frac{1}{S-1} \, \sum_{ij} (I_1[i, j] - \mu_1)^2}}
we obtain:
\eqline{R(I_0,I_1) \deq \frac{1}{S-1} \, \sum_{ij} \left(\frac{I_0[i, j] - \mu_0}{\sigma_0}\right) \, \left(\frac{I_1[i, j] - \mu_1}{\sigma_1}\right) = \frac{S \, m_{01}^{11} - m_{01}^{10} \, m_{01}^{01}}{\sqrt{(S \, m_{01}^{20} - (m_{01}^{10})^2) \, (S \, m_{01}^{02} - (m_{01}^{01})^2)}} \in [-1, 1]}
writing $m_{01}^{pq} = \sum_{ij} I_0[i, j]^p \, I_1[i, j]^q$, thus 
$\mu_0 = m_{10} / m_{00}$, $\mu_1 = m_{01} / m_{00}$, 
$\sigma_0^2 = (m_{20} - m_{10}^2 / m_{00})/ (m_{00} - 1)$, 
$\sigma_1^2 = (m_{02} - m_{01}^2 / m_{00})/ (m_{00} - 1)$.

It is \href{https://en.wikipedia.org/wiki/Correlation_and_dependence#Definition}{known} to be symmetric and normalized $-1 \leq R(I_0,I_1) \leq 1$.

It is related to the Euclidean distance between the two normalized images:
\eqline{D(I_0,I_1) \deq \frac{1}{S-1} \, \sum_{ij} \left(\frac{I_0[i, j] - \mu_0}{\sigma_0} - \frac{I_1[i, j] - \mu_1}{\sigma_1} \right)^2 = 2 \, \left(1 - R(I_0,I_1) \right) \in [0, 4]}

\newpage \subsubsection*{Incremental affine transformation}

- The transformation is a general or specialized affine transform written:
\eqline{\left(\begin{array}{c} i \\ j \end{array}\right) \leftarrow \left(\begin{array}{c} translation_i \\ translation_j \end{array}\right) + \left(\begin{array}{cc} zoom + warp & -rotation + twist \\ rotation + twist & zoom - warp \end{array}\right) \, \left(\begin{array}{c} i \\ j \end{array}\right)}
where:
\\ - $horizontal$: considers only an horizontal translation, 1 parameter,
\\ - $translation$: considers a 2D horizontal and vertical translation, 2 parameters,
\\ - $rigid$: considers translation and a (local) 2D rotation, 3 parameters,
\\ - $similarity$: considers translation, rotation and a scale factor change (i.e., a zoom), 4 parameters,
\\ - $affine$: considers translation, rotation, zoom and warp and twist shears, i.e., a 6 parameters affine transform.
\end{document}
